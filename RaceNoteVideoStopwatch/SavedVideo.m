//
//  SavedVideo.m
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "SavedVideo.h"


@implementation SavedVideo

@dynamic bestLap;
@dynamic laps;
@dynamic videoId;
@dynamic round;
@dynamic videoDate;
@dynamic track;
@dynamic title;
@dynamic url;
@dynamic latitude;
@dynamic longitude;
@dynamic videoTimePoints;
@dynamic cautions;
@dynamic overallTimePoints;

@end
