//
//  SavedVideo.h
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SavedVideo : NSManagedObject

@property (nonatomic, retain) NSNumber * bestLap;
@property (nonatomic, retain) NSString * laps;
@property (nonatomic, retain) NSDate * videoId;
@property (nonatomic, retain) NSString * round;
@property (nonatomic, retain) NSDate * videoDate;
@property (nonatomic, retain) NSString * track;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * cautions;
@property (nonatomic, retain) NSString * videoTimePoints;
@property (nonatomic, retain) NSString * overallTimePoints;

@end
