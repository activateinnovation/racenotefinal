//
//  VideoNote.m
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 12/6/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "VideoNote.h"


@implementation VideoNote

@dynamic videoId;
@dynamic type;
@dynamic time;
@dynamic note;
@dynamic title;
@dynamic position;

@end
