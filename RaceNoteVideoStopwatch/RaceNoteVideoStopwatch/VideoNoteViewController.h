//
//  VideoNoteViewController.h
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 12/6/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "VideoNote.h"

@interface VideoNoteViewController : UIViewController <UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

- (IBAction)backButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *positionTestField;
@property (strong, nonatomic) IBOutlet UITextField *timeTextField;
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
- (IBAction)nextOnKeyboard:(id)sender;

-(void)getVideoData: (NSDate *)videoId : (double)time :(int) tag;
@property double totalTime;

@property (strong, nonatomic) VideoNote *videoNote;
- (IBAction)saveAndContinue:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelButtonClicked;
- (IBAction)cancelButtonClicked:(id)sender;
@property int typeTag;
@property (strong, nonatomic) NSDate *videoId;
- (IBAction)tapOnScreen:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic) CGRect navBarFrame;
@property (nonatomic) CGRect topViewFrame;
@property (nonatomic) CGRect mainViewFrame;
@property (nonatomic) CGRect textViewFrame;

@property BOOL hasVideoNote;


@property (strong, nonatomic) NSMutableArray *positionArray;
@property (strong, nonatomic) UIPickerView *pickerView;

@end
