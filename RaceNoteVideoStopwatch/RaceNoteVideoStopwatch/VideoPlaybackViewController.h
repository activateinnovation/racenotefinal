//
//  VideoPlaybackViewController.h
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import "SavedVideo.h"
#import "MoreInfoViewController.h"


@interface VideoPlaybackViewController : UIViewController

-(void) getVideoData: (NSDate *)videoId :(ALAsset *)videoAsset;
@property (strong, nonatomic) ALAsset *asset;
@property (strong, nonatomic) MPMoviePlayerController *playerController;
@property (strong, nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerItem *playerItem;
- (IBAction)playVideo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *playButtonOutlet;
@property (strong, nonatomic) SavedVideo *savedVideo;

- (IBAction)transitionToInfo:(id)sender;

- (IBAction)goToMyRaces:(id)sender;
@property BOOL videoPlaybackOnorOff;

@property (strong, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *bestLapLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentLapLabel;
@property NSTimeInterval time;
@property NSTimeInterval currentInterval;
@property double totalTime;
@property (strong, nonatomic) NSString *timeString;

@property (strong, nonatomic) NSMutableArray *lapsArray;
@property (strong, nonatomic) NSMutableArray *videoTimePoints;
@property (strong, nonatomic) NSMutableArray *cautionsArray;
@property (strong, nonatomic) NSMutableArray *overallTags;
@property (strong, nonatomic) NSMutableArray *overallTimePoints;
@property (strong, nonatomic) NSMutableArray *lapIntervals;


@property (strong, nonatomic) IBOutlet UISlider *seekBar;
@property (strong, nonatomic) IBOutlet UIView *playBackControllerView;

- (IBAction)endDragging:(id)sender;
- (IBAction)beginDragging:(id)sender;
- (IBAction)drag:(id)sender;

@property int counter;

- (IBAction)addTimePointToVideo:(id)sender;

@end
