//
//  RaceInfoViewController.m
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "RaceInfoViewController.h"

@interface RaceInfoViewController ()

@end

@implementation RaceInfoViewController
@synthesize titleOutlet;
@synthesize trackOutlet;
@synthesize roundOutlet;
@synthesize dateOutlet;
@synthesize assetToSend;
@synthesize pickerView;
@synthesize roundArray;
@synthesize trackArray;
@synthesize tagger;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.trackArray = [[NSMutableArray alloc] initWithObjects:@"Eldora Speedway", @"Florence Speedway", @"I-80 Speedway", nil];
    
    self.roundArray = [[NSMutableArray alloc] initWithObjects:@"Practice", @"Hot Laps", @"Time Trial", @"Qualifier", @"Consolation", @"A Feature", nil];
    
    self.assetToSend = [[ALAsset alloc] init];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    
    if(self.tagger == YES){
        
        
        
    }
    else{
      
            CurrentVideo *currentVideoTemp = [CurrentVideo MR_findFirst];
           NSLog(@"Current Video OVerallTime: %@", currentVideoTemp.overallTimePoints);
        NSMutableArray *array = [[currentVideoTemp.laps componentsSeparatedByString:@","] mutableCopy];
        double totalTime = 0;
        for(int x = 0; x < array.count; x++)
        {
            totalTime += [[array objectAtIndex:x] doubleValue];
            
        }
        
        int minutes = (int)(totalTime / 60.0);
        int seconds = (int)(totalTime = totalTime - (minutes * 60));
        int milliseconds = (int)(totalTime * 100) % 100;
        // NSLog(@"Milliseconds: %02d", milliseconds);
        
        NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02i", minutes, seconds, milliseconds];
        
        [self.timeTextField setText:currentTimeString];

        
        NSLog(@"total time: %f", totalTime);
        
        
        if(currentVideoTemp)
        {
            NSLog(@"CurrentVideo: %@", currentVideoTemp);
            if(currentVideoTemp.title)
            {
                self.titleOutlet.text = currentVideoTemp.title;
            }
            
            if(currentVideoTemp.track)
            {
                self.trackOutlet.text = currentVideoTemp.track;
            }
            
            if(currentVideoTemp.round)
            {
                self.roundOutlet.text = currentVideoTemp.round;
            }
            NSDateFormatter *dateformate =[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"MM/dd/yyyy    hh:mma"]; // Date formater
            
            NSString *date;
            if(self.currentVideo.videoId){
                date  = [dateformate stringFromDate:self.currentVideo.videoId];
            }
            else{
                date  = [dateformate stringFromDate: [NSDate date]];
            }
            
            
            [self.dateOutlet setText:date];

        }
        else{
            
            NSDateFormatter *dateformate =[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"MM/dd/yyyy    hh:mma"]; // Date formater
            NSString *date = [dateformate stringFromDate:[NSDate date]];
            [self.dateOutlet setText:date];
        }

        [self.saveRaceDetails setHidden:YES];
        [self.cancelButton setHidden:YES];
    }
   
    
    UIView *orangeBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [orangeBar setBackgroundColor:[UIColor colorWithRed:(241/255.f) green:(106/255.f) blue:(39/255.f) alpha:1.0]];
    [self.view addSubview: orangeBar];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIView *horizontal1 = [[UIView alloc] initWithFrame:CGRectMake(5, 152, self.view.frame.size.width - 10, 1)];
    [horizontal1 setBackgroundColor:[UIColor colorWithRed:(227/255.f) green:(220/255.f) blue:(213/255.f) alpha:1.0]];
    [self.view addSubview:horizontal1];
    [self.view bringSubviewToFront:horizontal1];
    
    UIView *horizontal2 = [[UIView alloc] initWithFrame:CGRectMake(5, 205, self.view.frame.size.width - 10, 1)];
    [horizontal2 setBackgroundColor:[UIColor colorWithRed:(227/255.f) green:(220/255.f) blue:(213/255.f) alpha:1.0]];
    [self.view addSubview:horizontal2];
    [self.view bringSubviewToFront:horizontal2];
    
    UIView *horizontal3 = [[UIView alloc] initWithFrame:CGRectMake(5, 257, self.view.frame.size.width - 10, 1)];
    [horizontal3 setBackgroundColor:[UIColor colorWithRed:(227/255.f) green:(220/255.f) blue:(213/255.f) alpha:1.0]];
    [self.view addSubview:horizontal3];
    [self.view bringSubviewToFront:horizontal3];

    [self setUpPickerView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) raceInfoViewFromCapture {
    
    NSLog(@"Race Info View From Capture");
    
    self.tagger = YES;
    [self.saveRaceDetails setHidden:NO];
    [self.cancelButton setHidden:NO];
    [self.timeTextField setHidden:YES];
    [self.timeLabelOutlet setHidden:YES];
    [self.reviewRaceButton setHidden:YES];
    [self.recordNewRace setHidden:YES];
    [self.myRaces setHidden:YES];
    
   // self.currentVideo = [CurrentVideo MR_createEntity];
    self.currentVideo = [CurrentVideo MR_findFirst];
    
  
    if(self.currentVideo)
    {
        if(self.currentVideo.title)
        {
            self.titleOutlet.text = self.currentVideo.title;
        }
        
        if(self.currentVideo.track)
        {
            self.trackOutlet.text = self.currentVideo.track;
        }
        
        if(self.currentVideo.round)
        {
            self.roundOutlet.text = self.currentVideo.round;
        }
        
        NSDateFormatter *dateformate =[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"MM/dd/yyyy    hh:mma"]; // Date formater
        
        NSString *date;
        if(self.currentVideo.videoId){
            date  = [dateformate stringFromDate:self.currentVideo.videoId];
        }
        else{
            date  = [dateformate stringFromDate: [NSDate date]];
        }
        
        
      
        [self.dateOutlet setText:date];

    }
    else{
        
        self.currentVideo = [CurrentVideo MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
        
        NSDateFormatter *dateformate =[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"MM/dd/yyyy    hh:mma"]; // Date formater
        NSString *date = [dateformate stringFromDate:[NSDate date]];
        [self.dateOutlet setText:date];
        
        

    }

  

    
}

//Either puts the first responder at the next text box when Next is clicked or dismiss the keyboard when Done is clicked
- (IBAction)keyboardDismiss:(id)sender {
    if(sender == self.titleOutlet){
        
        //[self.titleOutlet resignFirstResponder];
        //[self.trackOutlet becomeFirstResponder];
    }
    else if(sender == self.trackOutlet){
        
        //[self.trackOutlet resignFirstResponder];
        //[self.roundOutlet becomeFirstResponder];
    }
    else if(sender == self.roundOutlet){
        
        //[self.roundOutlet resignFirstResponder];
    }
}

- (IBAction)tapGestureDismissKeyboard:(id)sender {
    
    [self.titleOutlet resignFirstResponder];
    [self.trackOutlet resignFirstResponder];
    [self.roundOutlet resignFirstResponder];
}

- (IBAction)goToReviewRace:(id)sender {
    
    [self saveAndGoToPage:(int)[sender tag]];
    
}

- (IBAction)goToRecordNewRace:(id)sender {
    
   [self saveAndGoToPage:(int)[sender tag]];
    
}

- (IBAction)goToMyRaces:(id)sender {
    
    [self saveAndGoToPage:(int)[sender tag]];
}


//Runs the saveVideo method to save the video and then transitions to the correct page based on which button was clicked by the user.
- (void) saveAndGoToPage: (int)tag  {
    
    NSDate *savedVideoId = [self saveVideo];
  
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        if(tag == 1){//Goes to the My races page with the new race added to the list
            
            NSLog(@"My Races");
            ViewController *infoView = [storyboard instantiateViewControllerWithIdentifier:@"MyRaces"];
            [self presentViewController:infoView animated:YES completion:nil];
        }
        else if(tag == 2){ //Got back to the video capture page
            NSLog(@"Record New Race");
            VideoCaptureViewController *videoCaptureView = [storyboard instantiateViewControllerWithIdentifier:@"VideoCapture"];
            [self presentViewController:videoCaptureView animated:YES completion:nil];
        }
        else{//Go to the VideoPlayback for this race to review it
            
             SavedVideo *savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue: savedVideoId];
            
            
            //VideoPlaybackViewController *videoPlaybackView = [storyboard instantiateViewControllerWithIdentifier:@"VideoPlayback"];
            //[self presentViewController:videoPlaybackView animated:YES completion:nil];
        
            [self getAssetForId:savedVideo.videoId];
           

            //NEED TO SAVE ASSET URL IN SAVED AND CURRENT VIDEO TO PULL ASSET TO SEND TO THIS VIEWf
            
        }
    
    NSArray *array = [CurrentVideo MR_findAll];
    for(int x = 0; x < array.count; x++)
    {
        
        CurrentVideo *temp = [array objectAtIndex:x];
        [temp MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
        
    }
   
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
        NSLog(@"Deleted");
    }];

    
}


-(void) getAssetForId: (NSDate *) videoId {
    
     ALAssetsLibrary *library = [self defaultAssetsLibrary];
    
 //__block ALAsset *asset = [[ALAsset alloc] init];
    
    __block ALAssetsGroup* groupToAddTo;
    [library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                           usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                               if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString: @"Race Note"]) {
                                   NSLog(@"found album %@", @"Race Note");
                                   groupToAddTo = group;
                                   
                                   [groupToAddTo enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                                       if(result)
                                       {
                                           
                                           NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                           [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                                           NSString *videoString = [dateFormatter stringFromDate:videoId];
                                           NSString *dateString = [dateFormatter stringFromDate:[result valueForProperty:ALAssetPropertyDate]];
                                      
                                           NSLog(@"DATESTRING:%@  \nVIDEOSTRING:%@", dateString, videoString);
                                           
                                           if([dateString isEqualToString: videoString]){
                                               self.assetToSend = result;
                                               NSLog(@"ASSSEST1: %@", self.assetToSend);
                                               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

                                              // SavedVideo *savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue: dateString];
                                               
                                               NSLog(@"Video Playback");
                                               VideoPlaybackViewController *videoPlaybackView = [storyboard instantiateViewControllerWithIdentifier:@"VideoPlayback"];
                                               

                                               //  [self performSelector:@selector(run:) withObject:[assetToSend valueForProperty:ALAssetPropertyDate] afterDelay:1.0];
                                               [self presentViewController:videoPlaybackView animated:YES completion:nil];
                                               
                                               [videoPlaybackView getVideoData: [assetToSend valueForProperty:ALAssetPropertyDate] : self.assetToSend];
                                               
                                               //[self getAssetForId:[assetToSend valueForProperty:ALAssetPropertyDate]];
                                               
                                           }
                                       
                                       }
                                   }];
                                
                                   // self.videoArray = [[[videoArray reverseObjectEnumerator] allObjects] mutableCopy];
                               }
                           }
                         failureBlock:^(NSError* error) {
                             NSLog(@"failed to enumerate albums:\nError: %@", [error localizedDescription]);
                         }];

    NSLog(@"ASSSEST2: %@", self.assetToSend);
}

- (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}



//Takes The Current Saved video and adds the information entered by the user and saves it to the Core Data for Saved Videos with all information.  It also deletes the Current Video so the next one can be recorded
-(NSDate *) saveVideo {
    
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
 
    CurrentVideo *currentVideoTemp = [CurrentVideo MR_findFirst];
    NSLog(@"CurrentVideoTemp: %@", currentVideoTemp);
    //SavedVideo *savedVideo = [SavedVideo MR_createEntityInContext:context];
   // SavedVideo *savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue: currentVideoTemp.videoId];
    SavedVideo *savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue:currentVideoTemp.videoId inContext:[NSManagedObjectContext MR_defaultContext]];
    NSLog(@"SavedVideoRaceInfo: %@", savedVideo);
    
    if(self.titleOutlet.text.length > 0){
        
        savedVideo.title = self.titleOutlet.text;
    }
    else{
        
        savedVideo.title = @"No Title Recorded";
    }
    
    if(self.trackOutlet.text.length > 0){
        
        savedVideo.track = self.trackOutlet.text;
    }
    else{
        
        savedVideo.track = @"No Track Recorded";
    }
    
    if(self.roundOutlet.text.length > 0){
        
        savedVideo.round = self.roundOutlet.text;
    }
    else{
        
        savedVideo.round = @"No Round Recorded";
    }
    
    NSLog(@"Saved Video: %@", savedVideo);
    
    savedVideo.videoDate = currentVideoTemp.videoDate;
    savedVideo.laps = currentVideoTemp.laps;
    savedVideo.bestLap = currentVideoTemp.bestLap;
    
    
    [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {

            
            NSLog(@"Saved SuccsessFully");
            [self.currentVideo MR_deleteEntity];
            [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                
                //successOrFail = success;

            }];
        }
    }];
    
    return savedVideo.videoId;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.trackOutlet){ //Track
      
        self.pickerView.tag = 0;
         self.trackOutlet.text = [self.trackArray objectAtIndex:0];
          [self.pickerView reloadAllComponents];
         [self.pickerView selectRow:0 inComponent:0 animated:NO];
    }
    else{// Round
        
        self.pickerView.tag = 1;
        self.roundOutlet.text = [self.roundArray objectAtIndex:0];
        [self.pickerView reloadAllComponents];
         [self.pickerView selectRow:0 inComponent:0 animated:NO];
        
    }
    //[textField resignFirstResponder];
    //[pickerView setHidden:NO];
}

#pragma mark PickerView Delegate
-(void) setUpPickerView {
    
    self.trackOutlet.delegate = self;
    self.roundOutlet.delegate = self;
    
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    
    
    UIView *toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    [toolbar setBackgroundColor:[UIColor lightGrayColor]];
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(self.mainView.frame.size.width - 100, 0, 100, 40)];
    [doneButton setTitle:@"Done" forState: UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(dismissPicker) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneButton];
    
    self.trackOutlet.inputView = self.pickerView;
    self.roundOutlet.inputView = self.pickerView;
    [self.trackOutlet setInputAccessoryView:toolbar];
    [self.roundOutlet setInputAccessoryView:toolbar];

}

-(void) dismissPicker{
    
    if([trackOutlet isFirstResponder]){
        
        [self.trackOutlet resignFirstResponder];
    }
    else if([roundOutlet isFirstResponder]){
        
        [self.roundOutlet resignFirstResponder];
    }
    
    if(self.pickerView.tag == 0)
    {
        
        self.trackOutlet.text = [self.trackArray objectAtIndex:[self.pickerView selectedRowInComponent:0]];
    }
    else{
        
        self.roundOutlet.text = [self.roundArray objectAtIndex:[self.pickerView selectedRowInComponent:0]];
    }
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    
    if(self.pickerView.tag == 0){
        
        return self.trackArray.count;
        
    }
    else{
        
        return self.roundArray.count;
    }
  
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if(self.pickerView.tag == 0)
    {
        
        return [self.trackArray objectAtIndex:row];
    }
    else{
        
        return [self.roundArray objectAtIndex:row];
    }
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
   
    
}

- (IBAction)saveDetails:(id)sender {
    
        NSLog(@"Save Details Clicked:");
    
    if(self.titleOutlet.text.length > 0){
        
        self.currentVideo.title = self.titleOutlet.text;
    }
    else{
        
        self.currentVideo.title = @"No Title Recorded";
    }
    
    if(self.trackOutlet.text.length > 0){
        
        self.currentVideo.track = self.trackOutlet.text;
    }
    else{
        
        self.currentVideo.track = @"No Track Recorded";
    }
    
    if(self.roundOutlet.text.length > 0){
        
        self.currentVideo.round = self.roundOutlet.text;
    }
    else{
        
        self.currentVideo.round = @"No Round Recorded";
    }
    
    self.currentVideo.videoId = [NSDate date];
    self.currentVideo.bestLap = 0;
    self.currentVideo.latitude = 0;
    self.currentVideo.longitude = 0;
    self.currentVideo.cautions = @"";
    self.currentVideo.videoTimePoints = @"";
    self.currentVideo.url = @"";
    self.currentVideo.overallTimePoints = @"";
    self.currentVideo.laps = @"";
    self.currentVideo.videoDate = [NSDate date];

    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];

    [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
             NSLog(@"Save Details Clicked Saved");
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            VideoCaptureViewController *videoCapture = [storyboard instantiateViewControllerWithIdentifier:@"VideoCapture"];
            [self presentViewController:videoCapture animated:YES completion:nil];
            NSLog(@"CURRENT Video Saved: %@", self.currentVideo);
        }
    }];
    
}
- (IBAction)cancelSetUp:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VideoCaptureViewController *videoCapture = [storyboard instantiateViewControllerWithIdentifier:@"VideoCapture"];
    [self presentViewController:videoCapture animated:YES completion:nil];

}

- (void)keyboardDidShow:(NSNotification *)note
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        
        if([self.titleOutlet isFirstResponder])
        {
            [self.navBar setFrame:CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y - 70, self.navBar.frame.size.width, self.navBar.frame.size.height)];
            [self.topView setFrame:CGRectMake(self.topView.frame.origin.x, self.topView.frame.origin.y - 70, self.topView.frame.size.width, self.topView.frame.size.height)];
            [self.mainView setFrame:CGRectMake(self.topView.frame.origin.x, self.mainView.frame.origin.y - 70, self.mainView.frame.size.width, self.mainView.frame.size.height)];
                    }
        else if([self.trackOutlet isFirstResponder])
        {
            [self.navBar setFrame:CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y - 140, self.navBar.frame.size.width, self.navBar.frame.size.height)];
            [self.topView setFrame:CGRectMake(self.topView.frame.origin.x, self.topView.frame.origin.y - 140, self.topView.frame.size.width, self.topView.frame.size.height)];
            [self.mainView setFrame:CGRectMake(self.topView.frame.origin.x, self.mainView.frame.origin.y - 140, self.mainView.frame.size.width, self.mainView.frame.size.height)];
            
        }
        else{ //TextView
            
            [self.navBar setFrame:CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y - 215, self.navBar.frame.size.width, self.navBar.frame.size.height)];
            [self.topView setFrame:CGRectMake(self.topView.frame.origin.x, self.topView.frame.origin.y - 215, self.topView.frame.size.width, self.topView.frame.size.height)];
            [self.mainView setFrame:CGRectMake(self.topView.frame.origin.x, self.mainView.frame.origin.y - 215, self.mainView.frame.size.width, self.mainView.frame.size.height)];
                        
        }
        
    } completion:nil];
}
@end
