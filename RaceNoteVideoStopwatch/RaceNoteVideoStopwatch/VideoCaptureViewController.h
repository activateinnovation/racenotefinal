//
//  VideoCaptureViewController.h
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AppDelegate.h"
#import <objc/message.h>
#import "SavedVideo.h"
#import "CurrentVideo.h"
#import "RaceInfoViewController.h"
#import "MoreInfoViewController.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <CoreLocation/CoreLocation.h>
#import "StartButton.h"
#import "LapButton.h"
#import "FinishButton.h"
#import "CautionButton.h"
#import "GreenFinishButton.h"
#import "CancelButton.h"
#import "VideoEncoder.h"
#import <Photos/Photos.h>
#import "UIView+Toast.h"

#define CAPTURE_FRAMES_PER_SECOND 30

@interface VideoCaptureViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVCaptureFileOutputRecordingDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, CLLocationManagerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate>

@property (strong, nonatomic) UIImagePickerController *videoRecorder;
@property BOOL WeAreRecording;
@property (strong, nonatomic) AVCaptureSession *CaptureSession;
@property (strong, nonatomic) AVCaptureMovieFileOutput *MovieFileOutput;
@property (strong, nonatomic) AVCaptureDeviceInput *VideoInputDevice;

@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;

- (void) CameraSetOutputProperties;
- (AVCaptureDevice *) CameraWithPosition:(AVCaptureDevicePosition) Position;

@property (strong, nonatomic) UILabel *startStopLabelOutlet;


@property (strong, nonatomic) IBOutlet UIButton *startStopButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (strong, nonatomic) IBOutlet UILabel *currentLapLabel;
@property (strong, nonatomic) IBOutlet UILabel *bestLapLabel;
@property (strong, nonatomic) NSTimer *raceTimer;

@property NSTimeInterval time;
@property (strong, nonatomic) IBOutlet UIImageView *recordingSymbol;

//- (IBAction)zoomInOut:(id)sender;
//- (IBAction)lapClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *lapButtonOutlet;
@property (strong, nonatomic) UILabel *lapLabelOutlet;
- (IBAction)raceInfoClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *raceInfoButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *myRacesButtonOutlet;
- (IBAction)myRacesButtonClicked:(id)sender;



@property BOOL zoomedIn; 


@property float effectiveScale;
@property CGPoint effectiveTranslation;
@property float effectiveRotationRadians;
@property float beginGestureRotationRadians;
@property CGPoint beginGestureTranslation;
@property float beginGestureScale;
@property (strong, nonatomic) UIView *CameraView;
@property (weak, nonatomic) IBOutlet UILabel *savingVideoLabel;
@property (weak, nonatomic) IBOutlet UIButton *zoomButtonOutlet;

//This array holds the laps for the current video, will be added to current video object when the video is over.
@property (strong, nonatomic) NSMutableArray *lapArray;
@property double bestLapTime;
@property double finalMarkTime;


//Custom buttons for the recording UI
@property (strong, nonatomic) LapButton *lapButton;
@property (strong, nonatomic) StartButton *startButton;
@property (strong, nonatomic) FinishButton *finishButton;
@property (strong, nonatomic) CautionButton *cautionButton;
@property (strong, nonatomic) GreenFinishButton *greenFinishButton;
@property (strong, nonatomic) CancelButton *cancelButton;
- (IBAction)addTimePointClicked:(id) sender;
@property (weak, nonatomic) IBOutlet UIButton *addTimePointOutlet;


@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;

-(void)startButtonPressed;
@property (weak, nonatomic) IBOutlet UIView *timerBar;

//The black alpha view
@property (strong, nonatomic) UIView *blackOutView;

//Stops the race and does not save any data
- (IBAction)undoStartRace:(id)sender;

@property dispatch_queue_t _captureQueue;

@property (strong, nonatomic) AVCaptureConnection* audioConnection;
@property (strong, nonatomic) AVCaptureConnection* videoConnection;

@property(strong, nonatomic) VideoEncoder* encoder;
//BOOL _isCapturing;
@property BOOL isPaused;

//Timer bar items
@property (weak, nonatomic) IBOutlet UILabel *bestLabel;
@property (weak, nonatomic) IBOutlet UIImageView *recordingSymbolView;
@property (weak, nonatomic) IBOutlet UILabel *currentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *stopwatch;
@property (weak, nonatomic) IBOutlet UIButton *undoStartRaceButton;

@property (strong, nonatomic) NSMutableArray *cautionArray;
@property (weak, nonatomic) IBOutlet UILabel *cautionLabel;
@property (weak, nonatomic) IBOutlet UILabel *touchStartLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cautionFlag;
@property (weak, nonatomic) IBOutlet UILabel *cautionTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *notRecordingView;

@property double cautionTime;
@property (strong, nonatomic) NSMutableArray *timePointsArray;
@property (strong, nonatomic) NSMutableArray *overallTimeStamps;
@property double startTime;

@property BOOL foundAlbum;
@end
