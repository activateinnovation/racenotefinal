//
//  CancelButton.m
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 10/28/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "CancelButton.h"

@implementation CancelButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // do my additional initialization here
        [self layoutSubviews];
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    // Get the size of the button
    CGRect bounds = self.bounds;
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height)];
    CAShapeLayer *circleLayer = [[CAShapeLayer alloc]init];
    circleLayer.fillColor = [UIColor clearColor].CGColor;
   // circleLayer.fillColor = [UIColor colorWithRed:(115/255.f) green:(110/255.f) blue:(113/255.f) alpha:0.3].CGColor;
    circleLayer.strokeColor = [UIColor colorWithRed:(211/255.f) green:(199/255.f) blue:(150/255.f) alpha:1.0].CGColor;
    circleLayer.path = bezierPath.CGPath;
    circleLayer.lineWidth = 4.0;
    
    UIImageView *lapTimer = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.origin.x + bounds.size.width/2 - 14, bounds.origin.y + bounds.size.height/4, 27, 27)];
    //[startFlag setBackgroundColor:[UIColor clearColor]];
    [lapTimer setImage:[UIImage imageNamed:@"left-arrow-faded.png"]];
    
    // Adding circle shapelayer as a subLayer to UILabel's layer
    UILabel *lapLabel = [[UILabel alloc] init];
    [lapLabel setText:@"Cancel"];
    [lapLabel setTextColor:[UIColor whiteColor]];
    
    [lapLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [lapLabel setTextAlignment:NSTextAlignmentCenter];
    [lapLabel setFrame:CGRectMake(bounds.origin.x + 5, bounds.origin.y + bounds.size.height/2.2, 80, 40)];
    [lapLabel setFont:[UIFont fontWithName:@"ProximaNova-BoldIt" size:18]];
    
    [self.layer addSublayer:circleLayer];
    
    
    [self addSubview: lapLabel];
    [self bringSubviewToFront:lapLabel];
    [self addSubview: lapTimer];
    [self bringSubviewToFront:lapTimer];
    
}

@end
