//
//  VideoNoteViewController.m
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 12/6/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "VideoNoteViewController.h"

@interface VideoNoteViewController ()

@end

@implementation VideoNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *orangeBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [orangeBar setBackgroundColor:[UIColor colorWithRed:(241/255.f) green:(106/255.f) blue:(39/255.f) alpha:1.0]];
    [self.view addSubview: orangeBar];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIView *horizontal1 = [[UIView alloc] initWithFrame:CGRectMake(5, 152, self.view.frame.size.width - 10, 1)];
    [horizontal1 setBackgroundColor:[UIColor colorWithRed:(227/255.f) green:(220/255.f) blue:(213/255.f) alpha:1.0]];
    [self.view addSubview:horizontal1];
    [self.view bringSubviewToFront:horizontal1];
    
    UIView *horizontal2 = [[UIView alloc] initWithFrame:CGRectMake(5, 202, self.view.frame.size.width - 10, 1)];
    [horizontal2 setBackgroundColor:[UIColor colorWithRed:(227/255.f) green:(220/255.f) blue:(213/255.f) alpha:1.0]];
    [self.view addSubview:horizontal2];
    [self.view bringSubviewToFront:horizontal2];
   
    UIView *vertical1 = [[UIView alloc] initWithFrame:CGRectMake(292, 156, 1, 44)];
    [vertical1 setBackgroundColor:[UIColor colorWithRed:(227/255.f) green:(220/255.f) blue:(213/255.f) alpha:1.0]];
    [self.view addSubview: vertical1];
    [self.view bringSubviewToFront: vertical1];

    self.positionArray = [[NSMutableArray alloc] init];
    for(int x = 1; x < 51; x++)
    {
        [self.positionArray addObject:[NSNumber numberWithInt:x]];
    }
    
    
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.positionTestField.inputView = self.pickerView;
    
    UIView *toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    [toolbar setBackgroundColor:[UIColor lightGrayColor]];
    
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(self.mainView.frame.size.width - 100, 0, 100, 40)];
    [doneButton setTitle:@"Done" forState: UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(dismissPicker) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneButton];
    
    [self.positionTestField setInputAccessoryView:toolbar];
    
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(71, 212, 450, 70)];
    [self.textView setBackgroundColor:[UIColor clearColor]];
    [self.textView setText:@"Enter additional notes..."];
    
    self.navBarFrame = CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y -10, self.navBar.frame.size.width, self.navBar.frame.size.height);
    self.topViewFrame = self.topView.frame;
    self.mainViewFrame = self.mainView.frame;
    self.textViewFrame = self.textView.frame;
    
    [self.view addSubview:self.textView];
    [self.view bringSubviewToFront:self.textView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    

    
    NSMutableArray *temp = [[VideoNote MR_findAll] mutableCopy];
    
   // NSLog(@"Temp: %@ VideoId: %@", [[temp objectAtIndex:0] videoId], self.videoId );
   
    NSMutableArray *tempNotesForVideo = [[NSMutableArray alloc] init];
    //Find notes by video
    for(int x = 0; x < temp.count; x++)
    {
        if([[temp objectAtIndex:x] videoId] == self.videoId)
        {
            [tempNotesForVideo addObject:[temp objectAtIndex:x]];
        }
    }
    
    //NSLog(@"TempNotesForVideo: %@", tempNotesForVideo);
    
    NSMutableArray *tempItemByTag = [[NSMutableArray alloc] init];
    for(int x = 0; x < tempNotesForVideo.count; x++)
    {
        VideoNote *tempNote = [tempNotesForVideo objectAtIndex:x];
        if([tempNote.type intValue] == self.typeTag){
            
            [tempItemByTag addObject:[tempNotesForVideo objectAtIndex:x]];
            
        }
        
    }
    
    // NSLog(@"TempByTag: %@", tempItemByTag);
    
   // NSLog(@"Time: %f:  self.totalTime: %f", [[NSString stringWithFormat:@"%.6f", [[[tempItemByTag objectAtIndex:0] time] doubleValue]] doubleValue], self.totalTime);
    
    //VideoNote *tempVideoNote = [VideoNote MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    self.videoNote = [VideoNote MR_createEntity];
    for(int x = 0; x < tempItemByTag.count; x++){
        
         //VideoNote *tempNote = [tempNotesForVideo objectAtIndex:x];
        if([[NSString stringWithFormat:@"%.6f", [[[tempItemByTag objectAtIndex:x] time] doubleValue]] doubleValue] == self.totalTime)
        {
           
            self.videoNote = [tempItemByTag objectAtIndex:x];
            //NSLog(@"Video note: %@", tempVideoNote);
            
        }
    }
    
    
     //NSLog(@"Video note: %@", tempVideoNote)
    
   
    if(tempItemByTag.count == 0 || temp.count == 0)
    {
        NSLog(@"Not Saved Already");
        int minutes = (int)(self.totalTime / 60.0);
        int seconds = (int)(self.totalTime = self.totalTime - (minutes * 60));
        int milliseconds = (int)(self.totalTime * 100) % 100;
        // NSLog(@"Milliseconds: %02d", milliseconds);
        
        NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02i", minutes, seconds, milliseconds];
        
        self.hasVideoNote = NO;
        self.positionTestField.text = @"";
        
        [self.timeTextField setText:currentTimeString];
        [self.textView setText:@"Enter additional notes..."];
    }
    else{
        
        NSLog(@"Save Video Note: %@", self.videoNote);
        
        self.hasVideoNote = YES;
        self.nameTextField.text = self.videoNote.title;
        if([self.videoNote.position isEqual:[NSNull null]] || self.videoNote.position == nil || [self.videoNote.position doubleValue] == 0)
           {
               self.positionTestField.text = @"";
           }
        else{
        
            self.positionTestField.text = [NSString stringWithFormat:@"%@", self.videoNote.position];
        }
        
        if([self.videoNote.note isEqualToString:@""] || self.videoNote.note == nil || self.videoNote.note.length == 0){
            
            [self.textView setText:@"Enter additional notes..."];

        }
        else{
             self.textView.text = self.videoNote.note;
        }
        
       
        
        int minutes = (int)(self.totalTime / 60.0);
        int seconds = (int)(self.totalTime = self.totalTime - (minutes * 60));
        int milliseconds = (int)(self.totalTime * 100) % 100;
        // NSLog(@"Milliseconds: %02d", milliseconds);
        
        NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02i", minutes, seconds, milliseconds];
        
        [self.timeTextField setText:currentTimeString];

    }
}

-(void) viewDidAppear:(BOOL)animated{

    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    if(self.hasVideoNote == NO)
    {
        self.positionTestField.text = @"";
    }
   
}

-(void) dismissPicker{
    
    [self.textView setHidden:YES];
    
    [self.positionTestField setText:[NSString stringWithFormat:@"%@",[self.positionArray objectAtIndex:[self.pickerView selectedRowInComponent:0]]]];
    [self.positionTestField resignFirstResponder];

    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        
        self.navBar.frame = self.navBarFrame;
        self.topView.frame = self.topViewFrame;
        self.mainView.frame = self.mainViewFrame;
         [self.textView setHidden:NO];
        [self.textView setFrame: CGRectMake(71, 212, 450, 70)];
        
    } completion:nil];

    [self.positionTestField resignFirstResponder];
    [self.textView setHidden:NO];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)nextOnKeyboard:(id)sender {
    
    if(sender == self.nameTextField)
    {
        [self.nameTextField resignFirstResponder];
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
  
        self.navBar.frame = self.navBarFrame;
        self.topView.frame = self.topViewFrame;
        self.mainView.frame = self.mainViewFrame;
        [self.textView setFrame: CGRectMake(71, 212, 450, 70)];
            } completion:nil];
        
    }
    else if(sender == self.positionTestField){
        
       // [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        [self.positionTestField setText:[NSString stringWithFormat:@"%@",[self.positionArray objectAtIndex:[self.pickerView selectedRowInComponent:0]]]];
        [self.positionTestField resignFirstResponder];

       /* self.navBar.frame = self.navBarFrame;
        self.topView.frame = self.topViewFrame;
        self.mainView.frame = self.mainViewFrame;
        [self.textView setFrame: CGRectMake(71, 212, 450, 70)];
        } completion:nil];*/
    }
    else
    {
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
      
        self.navBar.frame = self.navBarFrame;
        self.topView.frame = self.topViewFrame;
        self.mainView.frame = self.mainViewFrame;
        [self.textView setFrame: CGRectMake(71, 212, 450, 70)];
              } completion:nil];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if([textView.text isEqualToString:@""])
        {
            [textView setText: @"Enter additional notes..."];
        }
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
      
        self.navBar.frame = self.navBarFrame;
        self.topView.frame = self.topViewFrame;
        self.mainView.frame = self.mainViewFrame;
             [self.textView setHidden:NO];
        [self.textView setFrame: CGRectMake(71, 212, 450, 70)];
              } completion:nil];
        return NO;
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    if([textView.text isEqualToString:@"Enter additional notes..."])
    {
        [textView setText: @""];
    }
}

-(void)getVideoData: (NSDate *)videoId : (double)time :(int) tag
{
    self.videoId = [[NSDate alloc] init];
    self.videoId = videoId;

    self.totalTime = time;
     NSLog(@"Date: %@ Time: %f Tag : %d", videoId, time, tag);
    self.typeTag = tag;
   
    
}

- (IBAction)backButtonClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)saveAndContinue:(id)sender {
    
    if(self.hasVideoNote){
        if(self.nameTextField.text.length == 0)
        {
            self.videoNote.title = @"No Name Recorded";
        }
        else{
            
            self.videoNote.title = self.nameTextField.text;
        }
        
        if(self.positionTestField.text.length == 0 || [self.positionTestField.text isEqualToString:@"(null)"])
        {
            self.videoNote.position = [NSNumber numberWithDouble:0];
        }
        else{
            
            self.videoNote.position = [NSNumber numberWithDouble:[self.positionTestField.text doubleValue]];
        }
        
        self.videoNote.time = [NSNumber numberWithFloat:self.totalTime];
        
        
        if(self.textView.text.length == 0 || [self.textView.text isEqualToString:@"Enter additional notes..."])
        {
            
            self.videoNote.note = @"No Note Recorded";
            
        }
        else{
            
            self.videoNote.note = self.textView.text;
        }
        
        self.videoNote.type = [NSNumber numberWithInt:self.typeTag];
        self.videoNote.videoId = self.videoId;
        
        NSLog(@"Video Note: %@", self.videoNote);
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            
            NSLog(@"Video Note created");
            [self.nameTextField resignFirstResponder];
            [self.positionTestField resignFirstResponder];
            [self.textView resignFirstResponder];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
    }
    else{
    
        
        
    VideoNote *noteToSave = [VideoNote MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    
    if(self.nameTextField.text.length == 0)
    {
        noteToSave.title = @"No Name Recorded";
    }
    else{
        
        noteToSave.title = self.nameTextField.text;
    }
    
    if(self.positionTestField.text.length == 0)
    {
        noteToSave.position = [NSNumber numberWithDouble:0];
    }
    else{
        
        noteToSave.position = [NSNumber numberWithDouble:[self.positionTestField.text doubleValue]];
    }
        
    noteToSave.time = [NSNumber numberWithFloat:self.totalTime];
    
    
    if(self.textView.text.length == 0 || [self.textView.text isEqualToString:@"Enter additional notes..."])
    {
        
        noteToSave.note = @"No Note Recorded";
        
    }
    else{
        
        noteToSave.note = self.textView.text;
    }
    
    noteToSave.type = [NSNumber numberWithInt:self.typeTag];
    noteToSave.videoId = self.videoId;
    
    NSLog(@"Video Note: %@", noteToSave);
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
        NSLog(@"Video Note created");
        [self.nameTextField resignFirstResponder];
        [self.positionTestField resignFirstResponder];
        [self.textView resignFirstResponder];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    }
}
- (IBAction)cancelButtonClicked:(id)sender
{
    
    [self.nameTextField resignFirstResponder];
    [self.positionTestField resignFirstResponder];
    [self.textView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)tapOnScreen:(id)sender
{
    
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
 
    self.navBar.frame = self.navBarFrame;
    self.topView.frame = self.topViewFrame;
    self.mainView.frame = self.mainViewFrame;
         [self.textView setHidden:NO];
    [self.textView setFrame: CGRectMake(71, 212, 450, 70)];
    } completion:nil];
    
    if([self.positionTestField isFirstResponder])
    {
        [self.positionTestField setText:[NSString stringWithFormat:@"%@",[self.positionArray objectAtIndex:[self.pickerView selectedRowInComponent:0]]]];
    }
    [self.nameTextField resignFirstResponder];
    [self.positionTestField resignFirstResponder];
    [self.textView resignFirstResponder];
    
    
    
}

- (void)keyboardDidShow:(NSNotification *)note
{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
 

    if([self.nameTextField isFirstResponder])
    {
    [self.navBar setFrame:CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y - 40, self.navBar.frame.size.width, self.navBar.frame.size.height)];
    [self.topView setFrame:CGRectMake(self.topView.frame.origin.x, self.topView.frame.origin.y - 40, self.topView.frame.size.width, self.topView.frame.size.height)];
    [self.mainView setFrame:CGRectMake(self.topView.frame.origin.x, self.mainView.frame.origin.y - 40, self.mainView.frame.size.width, self.mainView.frame.size.height)];
    [self.textView setFrame:CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y - 40, self.textView.frame.size.width, self.textView.frame.size.height)];
    }
    else if([self.positionTestField isFirstResponder])
    {
        [self.navBar setFrame:CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y - 120, self.navBar.frame.size.width, self.navBar.frame.size.height)];
        [self.topView setFrame:CGRectMake(self.topView.frame.origin.x, self.topView.frame.origin.y - 120, self.topView.frame.size.width, self.topView.frame.size.height)];
        [self.mainView setFrame:CGRectMake(self.topView.frame.origin.x, self.mainView.frame.origin.y - 120, self.mainView.frame.size.width, self.mainView.frame.size.height)];
        [self.textView setFrame:CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y - 120, self.textView.frame.size.width, self.textView.frame.size.height)];
        [self.textView setHidden:YES];
    }
    else{ //TextView
        
        [self.navBar setFrame:CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y - 180, self.navBar.frame.size.width, self.navBar.frame.size.height)];
        [self.topView setFrame:CGRectMake(self.topView.frame.origin.x, self.topView.frame.origin.y - 180, self.topView.frame.size.width, self.topView.frame.size.height)];
        [self.mainView setFrame:CGRectMake(self.topView.frame.origin.x, self.mainView.frame.origin.y - 180, self.mainView.frame.size.width, self.mainView.frame.size.height)];
        [self.textView setFrame:CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y - 180, self.textView.frame.size.width, self.textView.frame.size.height)];
        
    }
        
            } completion:nil];
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    
    return self.positionArray.count;
   
    
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    
        return [NSString stringWithFormat:@"%@", [self.positionArray objectAtIndex:row]];
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
     //   self.positionTestField.text = [NSString stringWithFormat:@"%@", [self.positionArray objectAtIndex:row]];
    


}
@end
