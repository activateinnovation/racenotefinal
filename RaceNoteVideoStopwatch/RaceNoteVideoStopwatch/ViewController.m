//
//  ViewController.m
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize videoArray;
@synthesize tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIView *orangeBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [orangeBar setBackgroundColor:[UIColor colorWithRed:(241/255.f) green:(106/255.f) blue:(39/255.f) alpha:1.0]];
    [self.view addSubview: orangeBar];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    

    
    self.videoArray = [[NSMutableArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    ALAssetsLibrary *library = [self defaultAssetsLibrary];
    
    
    __block ALAssetsGroup* groupToAddTo;
    [library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                           usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                               if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString: @"Race Note"]) {
                                   NSLog(@"found album %@", @"Race Note");
                                   groupToAddTo = group;
                                   
                                   [groupToAddTo enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                                       if(result)
                                       {
                                           
                                           [videoArray addObject:result];
                                           NSLog(@"Inserted");
                                            NSLog(@"VideoArray: %@", videoArray);
                                       }
                                   }];
                                   
                                  // self.videoArray = [[[videoArray reverseObjectEnumerator] allObjects] mutableCopy];
                                   
                               }
                           }
                         failureBlock:^(NSError* error) {
                             NSLog(@"failed to enumerate albums:\nError: %@", [error localizedDescription]);
                         }];
    
    if(self.videoArray.count == 0){
        
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy"]; // Date formater
        [self.yearLabel setText:[dateformate stringFromDate:[NSDate date]]];
    }
    else{
        
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
    }
    

}

-(void) viewDidAppear:(BOOL)animated{
       [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

#pragma mark start of TABLEVIEW DELEGATE


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.videoArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
    }
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
  
    
    //The Date of this asset will pull infortmation from stored Video Objects that have more info like the date as the key, the url, the number of the movie, any laps or cautions.
    SavedVideo *savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue:  [[self.videoArray objectAtIndex:indexPath.row] valueForProperty:ALAssetPropertyDate]];
    

    NSLog(@"Saved Video: %@", savedVideo);
    
    
    UILabel *trackLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 4, 500, 30)];
     [trackLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:20]];
    [trackLabel setText:savedVideo.track];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 34, 300, 30)];
    [titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:17]];
    [titleLabel setText:savedVideo.title];
    
    UILabel *roundLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.size.width/2 + 110, cell.frame.size.height/2 - 3, 300, 30)];
    [roundLabel setFont:[UIFont systemFontOfSize:18]];
    [roundLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:17]];
    [roundLabel setText:savedVideo.round];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 135, cell.frame.size.height/2 - 3, 150, 30)];
    [dateLabel setTextColor:[UIColor darkGrayColor]];
    [dateLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:17]];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"MM/dd/yyyy"]; // Date formater
    NSString *date = [dateformate stringFromDate:savedVideo.videoId];
    [dateLabel setText: date];
    
    NSDateFormatter *dateformateYear =[[NSDateFormatter alloc]init];
    [dateformateYear setDateFormat:@"yyyy"]; // Date formater
    [self.yearLabel setText:[dateformateYear stringFromDate: savedVideo.videoId]];
    
    NSLog(@"Lat: %@, %@", savedVideo.latitude, savedVideo.longitude);
    
    [cell addSubview:titleLabel];
    [cell addSubview:trackLabel];
    [cell addSubview:roundLabel];
    [cell addSubview:dateLabel];
    
    return cell;
}

//DELEGATE Method for tabelView that is run whenever a user clicks on a menu tab
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     SavedVideo *savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue:  [[self.videoArray objectAtIndex:indexPath.row] valueForProperty:ALAssetPropertyDate]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VideoPlaybackViewController *infoView = [storyboard instantiateViewControllerWithIdentifier:@"VideoPlayback"];
    [self presentViewController:infoView animated:YES completion:nil];
    
    [infoView getVideoData: savedVideo.videoId : [self.videoArray objectAtIndex:indexPath.row]];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
    
}


- (IBAction)recordRace:(id)sender {
    NSLog(@"Clicked");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VideoCaptureViewController *videoCaptureView = [storyboard instantiateViewControllerWithIdentifier:@"VideoCapture"];
    [self presentViewController:videoCaptureView animated:YES completion:nil];
    
}
@end
