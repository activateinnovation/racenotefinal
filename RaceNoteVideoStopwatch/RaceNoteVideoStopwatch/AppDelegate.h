//
//  AppDelegate.h
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoCaptureViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
