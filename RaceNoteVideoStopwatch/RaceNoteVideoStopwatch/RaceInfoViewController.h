//
//  RaceInfoViewController.h
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "CurrentVideo.h"
#import "SavedVideo.h"
#import "ViewController.h"
#import "VideoCaptureViewController.h"
#import "VideoPlaybackViewController.h"


@interface RaceInfoViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *titleOutlet;
@property (strong, nonatomic) IBOutlet UITextField *trackOutlet;
@property (strong, nonatomic) IBOutlet UITextField *roundOutlet;
@property (strong, nonatomic) IBOutlet UILabel *dateOutlet;
@property (strong, nonatomic) IBOutlet UITextField *timeTextField;
@property (strong, nonatomic) IBOutlet UILabel *timeLabelOutlet;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIView *mainView;

@property BOOL tagger;
//Arrays to fill the picker view
@property (strong, nonatomic) NSMutableArray *roundArray;
@property (strong, nonatomic) NSMutableArray *trackArray;

-(void) raceInfoViewFromCapture;

- (IBAction)keyboardDismiss:(id)sender;
- (IBAction)tapGestureDismissKeyboard:(id)sender;

- (IBAction)goToReviewRace:(id)sender;
- (IBAction)goToRecordNewRace:(id)sender;
- (IBAction)goToMyRaces:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *reviewRaceButton;
@property (strong, nonatomic) IBOutlet UIButton *recordNewRace;
@property (strong, nonatomic) IBOutlet UIButton *myRaces;

@property (strong, nonatomic) IBOutlet UIButton *saveRaceDetails;
- (IBAction)saveDetails:(id)sender;

@property (strong, nonatomic) UIPickerView *pickerView;

- (IBAction)cancelSetUp:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) ALAsset *assetToSend;

@property (strong, nonatomic) CurrentVideo *currentVideo;

@end