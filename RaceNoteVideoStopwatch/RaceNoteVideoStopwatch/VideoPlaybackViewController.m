//
//  VideoPlaybackViewController.m
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "VideoPlaybackViewController.h"

@interface VideoPlaybackViewController (){

id mTimeObserver;
    BOOL isSeeking;
float mRestoreAfterScrubbingRate;
}
@end

@implementation VideoPlaybackViewController
@synthesize asset;
@synthesize player;
@synthesize playerItem;
@synthesize playButtonOutlet;
@synthesize savedVideo;
@synthesize videoPlaybackOnorOff;
@synthesize imageView;
@synthesize time;
@synthesize totalTime;
@synthesize lapsArray, videoTimePoints, cautionsArray, overallTags, overallTimePoints;
@synthesize seekBar;
@synthesize timeString;
@synthesize playBackControllerView;
@synthesize lapIntervals;

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    ALAssetRepresentation *fileName = [self.asset defaultRepresentation];
    
    NSURL *fileURL = [fileName url];//[[NSBundle mainBundle]
    //URLForResoaurce:fileName withExtension:@".MOV"];
    NSLog(@"FileURL : %@", fileURL);
    
    self.videoPlaybackOnorOff = NO;
    
    self.player = [AVPlayer playerWithURL:fileURL];
    
    [self.player addObserver:self forKeyPath:@"status" options: 0 context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
    
    UIView *CameraView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height)];
    [[self view] addSubview:CameraView];
    [self.view sendSubviewToBack:CameraView];
    
    //[self.avPlayerLayer setPlayer:self.player];
    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    [playerLayer setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [CameraView.layer addSublayer:playerLayer];
    
    UIView *orangeBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [orangeBar setBackgroundColor:[UIColor colorWithRed:(241/255.f) green:(106/255.f) blue:(39/255.f) alpha:1.0]];
    [self.view addSubview: orangeBar];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    self.counter = 0;
    
     [self setUpOverlayUI];
    isSeeking = NO;
  //  [self initScrubberTimer];
    
    // [self syncPlayPauseButtons];
    [self syncScrubber];

   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //yMPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:[asset valueForProperty:ALAssetPropertyAssetURL]];
    
    
   
    
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self.player pause];
    self.videoPlaybackOnorOff = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getVideoData: (NSDate *)videoId :(ALAsset *)videoAsset
{
    
    self.lapsArray = [[NSMutableArray alloc] init];
    self.videoTimePoints = [[NSMutableArray alloc] init];
    self.cautionsArray = [[NSMutableArray alloc] init];
    self.overallTags    = [[NSMutableArray alloc] init];
    self.overallTimePoints = [[NSMutableArray alloc] init];
    self.lapIntervals = [[NSMutableArray alloc] init];
    
    self.asset = videoAsset;
    
    self.totalTime = [[asset valueForProperty:ALAssetPropertyDuration] doubleValue];
    self.seekBar.maximumValue = totalTime;
    self.seekBar.value = 0;

    NSLog(@"TotalTime: %f", totalTime);
    
    self.savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue: videoId];
   
    ALAssetRepresentation *fileName = [self.asset defaultRepresentation];
    
    NSURL *fileURL = [fileName url];
    
    self.player = [AVPlayer playerWithURL:fileURL];
    
    

    NSLog(@"Saved Video %@", self.savedVideo);

   CGImageRef imgRef = [[asset defaultRepresentation] fullResolutionImage];
    NSLog(@"imgRef %@", imgRef);
    UIImage* uiImage = [[UIImage alloc] initWithCGImage:imgRef];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height)];
     [imageView setImage:uiImage];
    [self.view addSubview:self.imageView];
    [self.view sendSubviewToBack:imageView];
    
    self.lapsArray = [[savedVideo.laps componentsSeparatedByString:@","] mutableCopy];
    self.videoTimePoints  = [[savedVideo.videoTimePoints componentsSeparatedByString:@","] mutableCopy];
    self.cautionsArray = [[savedVideo.cautions componentsSeparatedByString:@","] mutableCopy];
    
    NSMutableArray *temp = [[savedVideo.overallTimePoints componentsSeparatedByString:@","] mutableCopy];
    for(int x = 0; x < temp.count; x+=2){
        
        [self.overallTags addObject:[temp objectAtIndex:x]];
        [self.overallTimePoints addObject:[temp objectAtIndex:x+1]];
        
    }
    
    NSLog(@"OverallTimePoints: %@  Overall Tags: %@", self.overallTimePoints, self.overallTags);

    self.counter = 0;
    
    double bestLap = [savedVideo.bestLap doubleValue];
    
    int minutes = (int)(bestLap / 60.0);
    int seconds = (int)(bestLap = bestLap - (minutes * 60));
    int milliseconds = (int)(bestLap * 100) % 100;
    // NSLog(@"Milliseconds: %02d", milliseconds);
    
    NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02i", minutes, seconds, milliseconds];
   
    [self.bestLapLabel setText:currentTimeString];
    
    [self setUpLapIntervals];
   
}


-(void) setUpLapIntervals{
    
    double totalTimeCount = 0;
    for(int x = 0; x < lapsArray.count; x++)
    {
        [self.lapIntervals addObject:[NSNumber numberWithDouble: totalTimeCount]];
        totalTimeCount += [[lapsArray objectAtIndex:x] doubleValue];
    }
    
    NSLog(@"LapsInterval: %@", self.lapIntervals);
}


-(void) setUpOverlayUI {
    
    UIImage *clearImage = [[UIImage alloc] init];
    [self.seekBar setMinimumTrackImage:clearImage forState:UIControlStateNormal];
    [self.seekBar setMaximumTrackImage:clearImage forState:UIControlStateNormal];
    
    UIView *hortizontalDivider1 = [[UIView alloc] initWithFrame:CGRectMake(-1,  self.seekBar.frame.size.height/2 -9, 2, 18)];
    [hortizontalDivider1 setBackgroundColor:[UIColor lightGrayColor]];
    [self.seekBar addSubview:hortizontalDivider1];
    
    double percentTotal = 0;
    double previousWidth = 1;
    
    for(int x = 0; x < overallTimePoints.count; x++){
        
        double percentage = 0;
        double timePointPercentage = 0;
        if([overallTags[x] intValue] == 0){
       
            percentage = ([overallTimePoints[x] doubleValue]/ totalTime);
            NSLog(@"Percentage: %f", percentage);
       
            percentTotal += percentage;
            int lapcount = 0;
            for(int j = x +1; j < overallTags.count; j++)
            {
                if([[self.overallTags objectAtIndex:j] doubleValue] == 0.0){
                    
                    lapcount++;
                    
                }
            }
            
            if(lapcount == 0){
                
                
                
                if(percentTotal < 0.98){
                    
                    percentage += (0.98 - percentTotal);
                    
                }
            }
            
        }
        else if([overallTags[x] intValue] == 2)
        {
            
            timePointPercentage = ([overallTimePoints[x] doubleValue]/totalTime);
              NSLog(@"PercentTimePoint: %f", timePointPercentage);
        }
    
    
   

        
      
    UIView *hortizontalDividerTemp = [[UIView alloc] initWithFrame:CGRectMake(previousWidth - 2,  self.seekBar.frame.size.height/2 -9, 2, 18)];
        [hortizontalDividerTemp setBackgroundColor:[UIColor lightGrayColor]];
        
        
        UIImageView *cautionImageView = [[UIImageView alloc] init];
        UIImageView *timePointImageView = [[UIImageView alloc] init];
        
        UIView *cautionMarker = [[UIView alloc] init];
        UIView *timePointMarker = [[UIView alloc] init];
        
    
        if([overallTags[x] intValue] == 0){ //Laps
            
            UIView *temp = [[UIView alloc] initWithFrame:CGRectMake(previousWidth, self.seekBar.frame.size.height/2 - 2, self.seekBar.frame.size.width * percentage, 3)];

            
            UIImageView *lapViewTemp = [[UIImageView alloc] initWithFrame:CGRectMake((temp.frame.origin.x + temp.frame.size.width/2 - 8), -8, 16, 16)];
            
            NSLog(@"Lap");
            [temp setBackgroundColor:[UIColor colorWithRed:(25/255.f) green:(151/255.f) blue:(197/255.f) alpha:1.0]];
           
            [lapViewTemp setImage:[UIImage imageNamed:@"lap-blue.png"]];
            
             previousWidth += temp.frame.size.width;
            
            if(x != 0){
                [self.seekBar addSubview:hortizontalDividerTemp];
            }
            
            [self.seekBar addSubview:temp];
            [self.seekBar sendSubviewToBack:temp];
            [self.seekBar addSubview:lapViewTemp];
            [self.seekBar bringSubviewToFront:lapViewTemp];
            
        }
        else if([overallTags[x] intValue] == 1){ //Caution
            
            NSLog(@"Caution");
            
            [cautionImageView setFrame:CGRectMake(previousWidth - 6, -10, 12, 12)];
            [cautionImageView setImage:[UIImage imageNamed:@"flag-yellow.png"        ]];
            [cautionMarker setFrame:CGRectMake(previousWidth, self.seekBar.frame.size.height/2 -9, 2, 18)];
            [cautionMarker setBackgroundColor:[UIColor colorWithRed:(245/255.f) green:(195/255.f) blue:(45/255.f) alpha:1.0]];
            
             previousWidth += cautionMarker.frame.size.width - 2;
            [self.seekBar addSubview:cautionMarker];
            [self.seekBar bringSubviewToFront:cautionMarker];
            [self.seekBar addSubview:cautionImageView];
            [self.seekBar bringSubviewToFront:cautionImageView];
         
           // previousWidth += 2;
            
        }
        else{ //Time Point
            
            NSLog(@"TIME: %f", self.seekBar.frame.size.width * timePointPercentage);
            
            [timePointMarker setFrame:CGRectMake((self.seekBar.frame.size.width * timePointPercentage), self.seekBar.frame.size.height/2 - 9, 2, 18)];
            [timePointMarker setBackgroundColor:[UIColor colorWithRed:(251/255.f) green:(97/255.f) blue:(23/255.f) alpha:1.0]];
            
            [timePointImageView setFrame:CGRectMake((self.seekBar.frame.size.width * timePointPercentage) - 4, 27, 9, 9)];
            [timePointImageView setImage:[UIImage imageNamed:@"note-xsm-orange.png"]];
            
            [self.seekBar addSubview:timePointMarker];
          [self.seekBar bringSubviewToFront:timePointMarker];
            [self.seekBar addSubview:timePointImageView];
            [self.seekBar bringSubviewToFront:timePointImageView];
            //previousWidth += 2;
        }
        
        
    }
    
    UIView *hortizontalDivider2 = [[UIView alloc] initWithFrame:CGRectMake(previousWidth - 2, self.seekBar.frame.size.height/2 -9, 2, 18)];
    [hortizontalDivider2 setBackgroundColor:[UIColor lightGrayColor]];
    [self.seekBar addSubview:hortizontalDivider2];
    
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    
    if (context == nil) {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                        
                       });
        return;
    }
    [super observeValueForKeyPath:keyPath ofObject:object
                           change:change context:context];
    return;
}


- (void)playerItemDidReachEnd:(NSNotification *)notification {
    NSLog(@"Reached The End");
    [self.player seekToTime:kCMTimeZero];
    [self.player pause];
    self.videoPlaybackOnorOff = NO;
    [self.currentLapLabel setText:@"00:00:00"];
    [self.playButtonOutlet setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
}



    
- (IBAction)playVideo:(id)sender {
    
    if(self.videoPlaybackOnorOff == NO){
        
        [self.view sendSubviewToBack:self.imageView];
        //[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(syncScrubber) userInfo:nil repeats:YES];
        [self initScrubberTimer];
       
       // time = [NSDate timeIntervalSinceReferenceDate];
        self.videoPlaybackOnorOff = YES;
        
         [self.player play];
        //[self updateTimer];
    
        [self.playButtonOutlet setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    }
    else{
        
        [self.player pause];
       
        self.videoPlaybackOnorOff = NO;
        [self.playButtonOutlet setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    }
   
}
- (IBAction)transitionToInfo:(id)sender {
    
    [self.player pause];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MoreInfoViewController *infoView = [storyboard instantiateViewControllerWithIdentifier:@"moreInfo"];
    [self presentViewController:infoView animated:YES completion:nil];
    
    [infoView getVideoData: savedVideo.videoId : self.asset];
    
}

- (IBAction)goToMyRaces:(id)sender {
    
    [self.player pause];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MoreInfoViewController *infoView = [storyboard instantiateViewControllerWithIdentifier:@"MyRaces"];
    [self presentViewController:infoView animated:YES completion:nil];

    
}

- (CMTime)playerItemDuration
{
    AVPlayerItem *playerItem1 = [self.player currentItem];
    if (playerItem1.status == AVPlayerItemStatusReadyToPlay)
    {
       
        return([playerItem1 duration]);
    }
    
    return(kCMTimeInvalid);
}

/*
-(void) updateTimer {
    
    if(videoPlaybackOnorOff == NO){
    
        return;
    }
    
   // NSLog(@"Update");
    int count = 0;
    self.counter++;
    
    if(self.counter < 20){
       
        count = 0;
    }
    else{
        
        count = [self checkInterval:self.seekBar.value];
    }
    
   // NSLog(@"Count: %d", count);
    
    NSTimeInterval elapsedTime = 0;
    if(count == 0)
    {
        elapsedTime = self.seekBar.value;
    }
    else{
        
        elapsedTime = self.seekBar.value - [[self.lapIntervals objectAtIndex:count] doubleValue];
    }
    
   // NSTimeInterval currentTime =  [NSDate timeIntervalSinceReferenceDate];
    //NSTimeInterval elapsedTime = currentTime - time;
    
   // NSLog(@"currentTimeString: %f", elapsedTime);
    
    // int hours = (int)(elapsedTime/ 3600);
    int minutes = (int)(elapsedTime / 60.0);
    int seconds = (int)(elapsedTime = elapsedTime - (minutes * 60));
    int milliseconds = (int)(elapsedTime * 100) % 100;
    // NSLog(@"Milliseconds: %02d", milliseconds);
    
    NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02i", minutes, seconds, milliseconds];
    
    [self.currentLapLabel setText: currentTimeString];
    
    
   // NSLog(@"CheckLap: %d", [self checkInterval:self.seekBar.value]);
    
    
    [self performSelector:@selector(updateTimer) withObject:self afterDelay:0.01];
    
}*/


-(int) checkInterval: (float) timeTemp {

    for(int x = 0; x < self.lapIntervals.count; x++)
    {
        if(x == self.lapIntervals.count - 1)
        {
            if(timeTemp > [[self.lapIntervals objectAtIndex:x] doubleValue] && timeTemp < self.totalTime){
                
                return x;
            }
        }
        else{
            
            if(timeTemp > [[self.lapIntervals objectAtIndex:x] doubleValue] && timeTemp < [[self.lapIntervals objectAtIndex:x+1] doubleValue]){
                
                return x;
            }
        }
    }
    
    return (int)(self.lapIntervals.count - 1);
}


-(void)initScrubberTimer
{
    double interval = .1f;
    
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration))
    {
        return;
    }
    double duration = CMTimeGetSeconds(playerDuration);\
     NSLog(@"Duration: %f", duration);
    if (isfinite(duration))
    {
        CGFloat width = CGRectGetWidth([self.seekBar bounds]);
        interval = 0.5f * duration / width;
    }
    
    /* Update the scrubber during normal playback. */
    __weak VideoPlaybackViewController *weakSelf = self;
    mTimeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(0.01, NSEC_PER_SEC)
                                                               queue:NULL /* If you pass NULL, the main queue is used. */
                                                          usingBlock:^(CMTime time)
                     {
                         [weakSelf syncScrubber];
                     }];
    }

/* Set the scrubber based on the player current time. */
- (void)syncScrubber
{
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration))
    {
        seekBar.minimumValue = 0.0;
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration))
    {
        float minValue = [self.seekBar minimumValue];
        float maxValue = [self.seekBar maximumValue];
        double timeTemp = CMTimeGetSeconds([self.player currentTime]);

        
        [self.seekBar setValue:(maxValue - minValue) * timeTemp / duration + minValue];
        
        
        if(videoPlaybackOnorOff == NO){
            
            return;
        }
        
        // NSLog(@"Update");
        int count = 0;
        self.counter++;
        
        if(self.counter < 20){
            
            count = 0;
        }
        else{
            
            count = [self checkInterval:self.seekBar.value];
        }
        
        // NSLog(@"Count: %d", count);
        
        NSTimeInterval elapsedTime = 0;
        if(count == 0)
        {
            elapsedTime = self.seekBar.value;
        }
        else{
            
            elapsedTime = self.seekBar.value - [[self.lapIntervals objectAtIndex:count] doubleValue];
        }
        
        // NSTimeInterval currentTime =  [NSDate timeIntervalSinceReferenceDate];
        //NSTimeInterval elapsedTime = currentTime - time;
        
        // NSLog(@"currentTimeString: %f", elapsedTime);
        
        // int hours = (int)(elapsedTime/ 3600);
        int minutes = (int)(elapsedTime / 60.0);
        int seconds = (int)(elapsedTime = elapsedTime - (minutes * 60));
        int milliseconds = (int)(elapsedTime * 100) % 100;
        // NSLog(@"Milliseconds: %02d", milliseconds);
        
        NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02i", minutes, seconds, milliseconds];
        
        [self.currentLapLabel setText: currentTimeString];
        
        
        // NSLog(@"CheckLap: %d", [self checkInterval:self.seekBar.value]);
        
      
    }
}

- (IBAction)endDragging:(id)sender {
    
    if (!mTimeObserver)
    {
        CMTime playerDuration = [self playerItemDuration];
        if (CMTIME_IS_INVALID(playerDuration))
        {
            return;
        }
        
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration))
        {
            CGFloat width = CGRectGetWidth([self.seekBar bounds]);
            double tolerance = 0.1f * duration / width;
            
            __weak VideoPlaybackViewController *weakSelf = self;
            mTimeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, NSEC_PER_SEC) queue:NULL usingBlock:
                             ^(CMTime time)
                             {
                                 [weakSelf syncScrubber];
                             }];
        }
    }
    
    if (mRestoreAfterScrubbingRate)
    {
        [self.player setRate:mRestoreAfterScrubbingRate];
        mRestoreAfterScrubbingRate = 0.f;
    }
}

- (IBAction)beginDragging:(id)sender {
    
    mRestoreAfterScrubbingRate = [self.player rate];
    [self.player setRate:0.f];
    
    /* Remove previous timer. */
    [self removePlayerTimeObserver];
}

- (IBAction)drag:(id)sender {
    
    if ([sender isKindOfClass:[UISlider class]] && !isSeeking)
    {
        isSeeking = YES;
        UISlider* slider = sender;
        
        CMTime playerDuration = [self playerItemDuration];
        if (CMTIME_IS_INVALID(playerDuration)) {
            return;
        }
        
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration))
        {
            float minValue = [slider minimumValue];
            float maxValue = [slider maximumValue];
            float value = [slider value];
            
            double timeTemp = duration * (value - minValue) / (maxValue - minValue);
            
            [self.player seekToTime:CMTimeMakeWithSeconds(timeTemp, NSEC_PER_SEC) completionHandler:^(BOOL finished) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    isSeeking = NO;
                });
            }];
        }
    }
}

-(void)removePlayerTimeObserver
{
    if (mTimeObserver)
    {
        [self.player removeTimeObserver:mTimeObserver];
        mTimeObserver = nil;
    }
}
- (IBAction)addTimePointToVideo:(id)sender {
    
    UIImageView *timePointImageView = [[UIImageView alloc] init];
    UIView *timePointMarker = [[UIView alloc] init];
    
    double value = self.seekBar.value;
    double timePointPercentage = (self.seekBar.value/totalTime);

    [timePointMarker setFrame:CGRectMake((self.seekBar.frame.size.width * timePointPercentage), self.seekBar.frame.size.height/2 - 9, 2, 18)];
    [timePointMarker setBackgroundColor:[UIColor colorWithRed:(251/255.f) green:(97/255.f) blue:(23/255.f) alpha:1.0]];
    
    [timePointImageView setFrame:CGRectMake((self.seekBar.frame.size.width * timePointPercentage) - 4, 27, 9, 9)];
    [timePointImageView setImage:[UIImage imageNamed:@"note-xsm-orange.png"]];
    
    [self.seekBar insertSubview:timePointMarker atIndex:2];
    [self.seekBar insertSubview:timePointImageView atIndex:3];
    
    [self.overallTags addObject:[NSNumber numberWithDouble:2.0]];
    [self.overallTimePoints addObject:[NSNumber numberWithDouble:value]];
    [self.videoTimePoints addObject:[NSNumber numberWithDouble:value]];
    
    if([self.savedVideo.videoTimePoints isEqualToString:@""]){
        
        self.savedVideo.videoTimePoints = [NSString stringWithFormat:@"%f", value];

    }
    else{
        
        self.savedVideo.videoTimePoints = [NSString stringWithFormat:@"%@,%f", self.savedVideo.videoTimePoints, value];
    
    }
    self.savedVideo.overallTimePoints = [NSString stringWithFormat:@"%@,%f, %f", self.savedVideo.overallTimePoints, 2.0, value];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
        NSLog(@"Saved Video");
    }];

}
@end
