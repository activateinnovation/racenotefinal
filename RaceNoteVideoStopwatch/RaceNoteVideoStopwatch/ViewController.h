//
//  ViewController.h
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>
#import "SavedVideo.h"
#import "VideoPlaybackViewController.h"
#import "VideoCaptureViewController.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *videoArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *yearLabel;



- (IBAction)recordRace:(id)sender;

@end
