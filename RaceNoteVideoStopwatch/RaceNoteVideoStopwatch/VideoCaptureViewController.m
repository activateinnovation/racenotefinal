//
//  VideoCaptureViewController.m
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "VideoCaptureViewController.h"

@interface VideoCaptureViewController ()
{
    int _channels;
    Float64 _samplerate;
    BOOL _discont;
    int _currentFile;
    CMTime _timeOffset;
    CMTime _lastVideo;
    CMTime _lastAudio;
    int _cx;
    int _cy;
}
@end


@implementation VideoCaptureViewController
@synthesize videoRecorder;
@synthesize WeAreRecording;
@synthesize CaptureSession;
@synthesize MovieFileOutput;
@synthesize VideoInputDevice;
@synthesize PreviewLayer;
@synthesize spinner;
@synthesize startStopButton;
@synthesize currentLapLabel;
@synthesize raceTimer;
@synthesize time;
@synthesize startStopLabelOutlet;
@synthesize lapButtonOutlet;
@synthesize lapLabelOutlet;
@synthesize raceInfoButtonOutlet;

@synthesize myRacesButtonOutlet;
@synthesize zoomedIn;
@synthesize effectiveScale;
@synthesize effectiveTranslation;
@synthesize effectiveRotationRadians;
@synthesize beginGestureRotationRadians;
@synthesize beginGestureScale;
@synthesize beginGestureTranslation;
@synthesize CameraView;
@synthesize bestLapLabel;
@synthesize lapArray;
@synthesize bestLapTime;
@synthesize finalMarkTime;
@synthesize savingVideoLabel;
@synthesize locationManager;
@synthesize currentLocation;
@synthesize startButton;
@synthesize lapButton;
@synthesize finishButton;
@synthesize cautionButton;
@synthesize addTimePointOutlet;
@synthesize timerBar;
@synthesize cancelButton;
@synthesize greenFinishButton;
@synthesize blackOutView;
@synthesize _captureQueue;
@synthesize isPaused;
@synthesize encoder;
@synthesize cautionArray;
@synthesize cautionFlag, cautionLabel, cautionTimeLabel, touchStartLabel;
@synthesize cautionTime;
@synthesize overallTimeStamps;
@synthesize timePointsArray;
@synthesize startTime;
@synthesize foundAlbum;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
   // self.startStopButton.hidden = YES;
    [spinner startAnimating];
    
    self.cautionArray = [[NSMutableArray alloc] init];
    self.overallTimeStamps = [[NSMutableArray alloc] init];
    self.timePointsArray = [[NSMutableArray alloc] init];
    
    self.foundAlbum = NO;
    
    UIView *orangeBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [orangeBar setBackgroundColor:[UIColor colorWithRed:(241/255.f) green:(106/255.f) blue:(39/255.f) alpha:1.0]];
    [self.view addSubview: orangeBar];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self setUpFinishButton];
    [self setUpLapButton];
    [self setUpStartButton];
    [self setUpCautionButton];
    [self setUpGreenFinishButton];
    [self setUpCancelButton];
    
    [self setCautionViewOn:NO];
    
    [self.greenFinishButton setHidden:YES];
    [self setUpRecordingView: NO];
    self.zoomedIn = NO;
    
    
    //self.bestLapLabel.hidden = YES;
   // self.currentLapLabel.hidden = YES;
    self.view.backgroundColor = [UIColor blackColor];
    [self.savingVideoLabel setHidden:YES];
    
    //This will be used to retrieve the users current location to add as a tag to each videos data
    self.currentLocation = [[CLLocation alloc] init];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     self.lapArray = [[NSMutableArray alloc] init];
    self.currentLapLabel.text = @"00:00:00";
    self.bestLapLabel.text = @"00:00:00";
    WeAreRecording = NO;
    videoRecorder = [[UIImagePickerController alloc] init];
    CaptureSession = [[AVCaptureSession alloc] init];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
    
    
    
    //Adds the green start button to the video capture interface
    

}

//Creates a StartButton object in the corner of the screen
- (void) setUpStartButton {
    
    self.startButton = [[StartButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 95, self.view.frame.size.height - 95, 200, 200)];
    [self.startButton setShowsTouchWhenHighlighted:YES];
    
    
    [self.view addSubview:startButton];
    [self.startButton addTarget:self action:@selector(startButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}


//Creates the UIButton for the lap with the proper labels and images
-(void) setUpLapButton {
    
    self.lapButton = [[LapButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 95, self.view.frame.size.height - 95, 200, 200)];
    [self.lapButton setShowsTouchWhenHighlighted:YES];
  
    
    [self.view addSubview:lapButton];
    [self.lapButton addTarget:self action:@selector(lapClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void) setUpGreenFinishButton{
    
    self.greenFinishButton = [[GreenFinishButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 80, self.view.frame.size.height/2 - 47, 90, 90)];
    [self.greenFinishButton setShowsTouchWhenHighlighted:YES];
    
    //[self.view addSubview:self.greenFinishButton];
    [self.greenFinishButton addTarget:self action:@selector(greenFinishedButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void) setUpFinishButton{
    
    self.finishButton = [[FinishButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 80, self.view.frame.size.height/2 - 47, 90, 90)];
    [self.finishButton setShowsTouchWhenHighlighted:YES];
    
    [self.view addSubview:self.finishButton];
    [self.finishButton addTarget:self action:@selector(finishedRecordingPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void) setUpCautionButton{
    
    self.cautionButton = [[CautionButton alloc] initWithFrame:CGRectMake(self.view.frame.origin.x - 10, self.view.frame.size.height/2 - 47, 90, 90)];
    [self.cautionButton setShowsTouchWhenHighlighted:YES];
    
    
    
    [self.view addSubview:self.cautionButton];
    [self.cautionButton addTarget:self action:@selector(cautionButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void) setUpCancelButton {
    
    self.cancelButton = [[CancelButton alloc] initWithFrame:CGRectMake(self.view.frame.origin.x - 10, self.view.frame.size.height/2 - 47, 90, 90)];
    [self.cancelButton setShowsTouchWhenHighlighted:YES];
    
   // [self.view addSubview:self.cautionButton];
    [self.cancelButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];

}


-(void) updateTimer {
    
    if(WeAreRecording == NO){
        
        return;
    }
    
    //NSLog(@"Update");
    
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elapsedTime = currentTime - time;
    
   // NSLog(@"currentTimeString: %f", elapsedTime);
    
   // int hours = (int)(elapsedTime/ 3600);
    int minutes = (int)(elapsedTime / 60.0);
    int seconds = (int)(elapsedTime = elapsedTime - (minutes * 60));
    int milliseconds = (int)(elapsedTime * 100) % 100;
   // NSLog(@"Milliseconds: %02d", milliseconds);
    
    NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02i", minutes, seconds, milliseconds];
    
    
    [self.currentLapLabel setText: currentTimeString];
    
    [self performSelector:@selector(updateTimer) withObject:self afterDelay:0.01];

}


-(void) viewDidAppear:(BOOL)animated{
    
    [self recordVideoButton];
    if ([self.PreviewLayer.connection isVideoOrientationSupported])
        [self.PreviewLayer.connection setVideoOrientation:(AVCaptureVideoOrientation)[[UIApplication sharedApplication] statusBarOrientation]];
    [spinner stopAnimating];
    //self.startStopButton.hidden = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return  interfaceOrientation == UIInterfaceOrientationLandscapeLeft
    || interfaceOrientation == UIInterfaceOrientationLandscapeRight ;
}

-(BOOL) prefersStatusBarHidden
{
    return NO;
}

-(void) viewWillDisappear:(BOOL)animated{

    [MovieFileOutput stopRecording];
    
}

- (NSUInteger)supportedInterfaceOrientations{
      // NSLog(@"HERE3");
    return UIInterfaceOrientationMaskLandscapeRight;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//********** RECORD VIDEO BUTTON **********

- (void) recordVideoButton
{
	//----- ADD INPUTS -----
	NSLog(@"Adding video input");

    
	//ADD VIDEO INPUT
	AVCaptureDevice *VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	if (VideoDevice)
	{
		NSError *error;
		VideoInputDevice = [AVCaptureDeviceInput deviceInputWithDevice:VideoDevice error:&error];
		if (!error)
		{
			if ([CaptureSession canAddInput:VideoInputDevice])
				[CaptureSession addInput:VideoInputDevice];
			else
				NSLog(@"Couldn't add video input");
		}
		else
		{
			NSLog(@"Couldn't create video input");
		}
	}
	else
	{
		NSLog(@"Couldn't create video capture device");
	}
	
	//ADD AUDIO INPUT
	NSLog(@"Adding audio input");
	AVCaptureDevice *audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
	NSError *error = nil;
	AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioCaptureDevice error:&error];
	if (audioInput)
	{
		[CaptureSession addInput:audioInput];
	}
	
	
	//----- ADD OUTPUTS -----
	
	//ADD VIDEO PREVIEW LAYER
	NSLog(@"Adding video preview layer");
	[self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:CaptureSession]];
    
		//<<SET ORIENTATION.  You can deliberatly set this wrong to flip the image and may actually need to set it wrong to get the right image
	
	[[self PreviewLayer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];
	
    
    //Add Capture Queue
    // create an output for YUV output with self as delegate
    _captureQueue = dispatch_queue_create("com.activateinnovation.racenote", DISPATCH_QUEUE_SERIAL);
    AVCaptureVideoDataOutput* videoout = [[AVCaptureVideoDataOutput alloc] init];
    [videoout setSampleBufferDelegate:self queue:_captureQueue];
   // NSDictionary* setcapSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                 //   [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange], kCVPixelBufferPixelFormatTypeKey,
                                   // nil];
    
    videoout.videoSettings = nil;
    //NSDictionary* actual = videoout.videoSettings;
   
    [CaptureSession addOutput:videoout];
    self.videoConnection = [videoout connectionWithMediaType:AVMediaTypeVideo];

    AVCaptureAudioDataOutput* audioout = [[AVCaptureAudioDataOutput alloc] init];
    [audioout setSampleBufferDelegate:self queue:_captureQueue];
    [CaptureSession addOutput:audioout];
    _audioConnection = [audioout connectionWithMediaType:AVMediaTypeAudio];

    [CaptureSession setSessionPreset:AVCaptureSessionPresetHigh];
	
	/*//ADD MOVIE FILE OUTPUT
	NSLog(@"Adding movie file output");
	MovieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
	
	Float64 TotalSeconds = 3600;			//Total seconds
	int32_t preferredTimeScale = 30;	//Frames per second
	CMTime maxDuration = CMTimeMakeWithSeconds(TotalSeconds, preferredTimeScale);	//<<SET MAX DURATION
	MovieFileOutput.maxRecordedDuration = maxDuration;
	
	MovieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024;						//<<SET MIN FREE SPACE IN BYTES FOR RECORDING TO CONTINUE ON A VOLUME
	
	if ([CaptureSession canAddOutput:MovieFileOutput])
		[CaptureSession addOutput:MovieFileOutput];
    
	//SET THE CONNECTION PROPERTIES (output properties)
	[self CameraSetOutputProperties];			//(We call a method as it also has to be done after changing camera)
    
	NSLog(@"Setting image quality");
	[CaptureSession setSessionPreset:AVCaptureSessionPresetMedium];
	if ([CaptureSession canSetSessionPreset:AVCaptureSessionPreset1280x720])		//Check size based configs are supported before setting them
		[CaptureSession setSessionPreset:AVCaptureSessionPreset1280x720];
    */
    
	
	//----- DISPLAY THE PREVIEW LAYER -----
	//Display it full screen under out view controller existing controls
    
  
	NSLog(@"Display the preview layer");
	CGRect layerRect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);//[[[self view] layer] bounds];
	[PreviewLayer setBounds:layerRect];
	[PreviewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),
                                          CGRectGetMidY(layerRect))];
    
    _cy = PreviewLayer.frame.size.width;
    _cx = PreviewLayer.frame.size.height;
    

    
   
    
    //[[[self view] layer] addSublayer:[[self CaptureManager] previewLayer]];
	//We use this instead so it goes on a layer behind our UI controls (avoids us having to manually bring each control to the front):
	self.CameraView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	[[self view] addSubview:CameraView];
	[self.view sendSubviewToBack:CameraView];
    //CameraView.layer.masksToBounds = YES;
    // do one time set-up of gesture recognizers
    UIGestureRecognizer *recognizer;
    
    recognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchFrom:)];
    recognizer.delegate = self;
    [self.CameraView addGestureRecognizer:recognizer];
    
    [[CameraView layer] addSublayer:PreviewLayer];

		
    
	//----- START THE CAPTURE SESSION RUNNING -----
	[CaptureSession startRunning];
    

}

-(void) CameraSetOutputProperties
{
	//SET THE CONNECTION PROPERTIES (output properties)
	AVCaptureConnection *CaptureConnection = [MovieFileOutput connectionWithMediaType:AVMediaTypeVideo];
	
	//Set landscape (if required)
	if ([CaptureConnection isVideoOrientationSupported])
	{
        
		AVCaptureVideoOrientation orientation = AVCaptureVideoOrientationLandscapeRight;		//<<<<<SET VIDEO ORIENTATION IF LANDSCAPE
		[CaptureConnection setVideoOrientation:orientation];
	}
	
}

//********** GET CAMERA IN SPECIFIED POSITION IF IT EXISTS **********
- (AVCaptureDevice *) CameraWithPosition:(AVCaptureDevicePosition) Position
{
	NSArray *Devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
	for (AVCaptureDevice *Device in Devices)
	{
		if ([Device position] == Position)
		{
			return Device;
		}
	}
	return nil;
}


//Starts the recording if the Start button is pressed either to a new file or to the same stream if the video input is paused
-(void) startButtonPressed
{
    [self.startButton setAlpha:0.35f];
    if(isPaused){
        
        [self saveCaution:self.cautionTime : [NSDate timeIntervalSinceReferenceDate]];
        [self resumeCapture];
        [self setCautionViewOn:NO];
        isPaused = NO;
    }
	if (!WeAreRecording)
	{
		//----- START RECORDING -----
		NSLog(@"START RECORDING");
		WeAreRecording = YES;
        
        
		//Show the correct views for recording
        [self setUpRecordingView:YES];
        time = [NSDate timeIntervalSinceReferenceDate];
        startTime = [NSDate timeIntervalSinceReferenceDate];
        //Starts timing
        [self updateTimer];
        
        
        @synchronized(self)
        { NSLog(@"starting capture");
                
                // create the encoder once we have the audio params
                self.encoder = nil;
                self.isPaused = NO;
                _discont = NO;
                _timeOffset = CMTimeMake(0, 0);
                self.WeAreRecording = YES;
        
        }

	}
	
    [self performSelector:@selector(resetStartButton) withObject:nil afterDelay:0.185];
}

-(void) resetStartButton {
    
    [self.startButton setAlpha:1.0f];
}

//This is called when the green finished or undo race buttons are clicked
- (void) stopCapture: (BOOL) saveCapture
{
    
    @synchronized(self)
    {
        if (self.WeAreRecording)
        {
            
            //This runs if the user cancels the recording
            //Resets the page to the default recording view ready to record again
            if(!saveCapture){
                [self.bestLapLabel setText:@"00:00:00"];
                [self.currentLapLabel setText:@"00:00:00"];
                [self.lapArray removeAllObjects];//Will Need to Remove the other types of objects
                WeAreRecording = NO;
                [self setUpRecordingView:NO];
                [self.view bringSubviewToFront:spinner];
                [self.view bringSubviewToFront:savingVideoLabel];
                [self.cancelButton setHidden:YES];
                [self.greenFinishButton setHidden:YES];
                
                //Stops the data stream
                dispatch_async(_captureQueue, ^{
                    [self.encoder finishWithCompletionHandler:^{
                        self.encoder = nil;
                    }];
                });
            }
            else{  //This is run if the video is being saved to the local DB and "Race Note" Album in Photos
                
                //Sets up the temporary file and URL for the video that will be saved
                NSString* filename = [NSString stringWithFormat:@"capture%d.mp4", _currentFile];
                NSString* path = [NSTemporaryDirectory() stringByAppendingPathComponent:filename];
                NSURL* url = [NSURL fileURLWithPath:path];
                _currentFile++;
                
                
                //Stops the video cand audio capture streams
                self.WeAreRecording = NO;
                dispatch_async(_captureQueue, ^{
                    [self.encoder finishWithCompletionHandler:^{
                        self.WeAreRecording = NO;
                        self.encoder = nil;
                        
                        //Sets up the variables that will be used throughout this block
                        __block ALAssetsGroup* groupToAddTo = [[ALAssetsGroup alloc] init];
                        __block ALAssetsLibrary *library = [self defaultAssetsLibrary];
                        __block int counter = 0;
                        
                        //Pulls all of the albums from the photos
                        PHFetchResult *userAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
                        
                        //Loops through all of the albums and tries to find the "Race Note" Folder
                        [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
                            NSLog(@"album title %@", collection.localizedTitle);
                            
                            //If it finds the Race Note folder then it runs this section
                            if([collection.localizedTitle isEqualToString:@"Race Note"])
                            {
                                
                                NSLog(@"Found Race Note Folder");
                                //Finds the folder that we need to add to
                                //Ups the counter so that a new album called Race Note is not created as a duplicate
                                counter++;
                                
                                //Loops through all of the Albums looking for the Race note folder as an ALAssetGroup Rather than a PHAssetCollection so that the video will be saved as an ALAsset
                                //There is much more documentation on ALAsset as it has been the standard since iOS6
                                [library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                                       usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                                           //Checks the Album Name
                                                           if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString: @"Race Note"]) {
                                                               
                                                               
                                                               NSLog(@"found album %@", @"Race Note");
                                                               
                                                               //Increases counter to stop from recreating the album and sets the ALAssetsGroup
                                                               counter++;
                                                               NSLog(@"Counter: %d", counter);
                                                               groupToAddTo = group;
                                                               NSLog(@"Group to Add To 1: %@", groupToAddTo);
                                                               
                                                               
                                                           }
                                                           else{
                                                               
                                                               //Do nothing here becasue it is refering to an album that is not the Race note Album
                                                               
                                                           }
                                                       }
                                                     failureBlock:^(NSError* error) {
                                                         NSLog(@"failed to enumerate albums:\nError: %@", [error localizedDescription]);
                                                     }];
                                
                            }//If the Race Note album is not found on this iteration then run this
                            else{
                                
                                
                                NSLog(@"Not The Race Note Album");
                                PHObjectPlaceholder *__block assetCollectionPlaceholder;
                                
                                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                                    
                                    //If the counter has not been incremented then we know that the album does not exsit so it ust br created using the Photos Framework provided as a Kit in iOS 8
                                    if(counter == 0){
                                        
                                        // Request creating an album from the textfield
                                        PHAssetCollectionChangeRequest *createAlbumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle: @"Race Note"];
                                        
                                        // Store placeholder object for use later
                                        assetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection;
                                    }
                                    
                                } completionHandler:^(BOOL success, NSError *error){
                                    
                                    //Loops through the ALAssetsGroups and kicks out if it finds the album so that the video can be saved
                                    __block int counter = 0;
                                    [library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                                           usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                                               if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString: @"Race Note"]) {
                                                                   
                                                                   NSLog(@"found album %@", @"Race Note");
                                                                   
                                                                   counter++;
                                                                   NSLog(@"Counter: %d", counter);
                                                                   groupToAddTo = group;
                                                                   NSLog(@"Group to Add To 2: %@", groupToAddTo);
                                                                   *stop = YES;
                                                                   
                                                               }
                                                               else{
                                                                   
                                                                   
                                                               }
                                                           }
                                                         failureBlock:^(NSError* error) {
                                                             NSLog(@"failed to enumerate albums:\nError: %@", [error localizedDescription]);
                                                         }];
                                    
                                    
                                }];
                                
                                
                            }
                            
                            
                        }];
                        
                        
                        
                        //Checks to make sure the video can be saved,  if it can the it proceeds to find the correct group to save to and writes the video to the URL
                        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:url])
                            
                        {
                            [library writeVideoAtPathToSavedPhotosAlbum:url  completionBlock:^(NSURL *assetURL, NSError *error)
                             {
                                 if (error)
                                 {
                                     
                                 }
                                 else{
                                     
                                     if (error.code == 0) {
                                         NSLog(@"saved image completed:\nurl: %@", assetURL);
                                         
                                         //Gets the video that was just saved to pull the id from it (the date)
                                         __block ALAsset *assetToSave = [[ALAsset alloc] init];
                                         // try to get the asset
                                         [library assetForURL:assetURL
                                                  resultBlock:^(ALAsset *asset) {
                                                      // assign the photo to the album
                                                      [groupToAddTo addAsset:asset];
                                                      NSDate *date = [asset valueForProperty:ALAssetPropertyDate];
                                                      NSLog(@"Asset Date%@", date);
                                                      // NSLog(@"Added %@ to %@", [[asset defaultRepresentation] filename], albumName);
                                                      NSString *finalLapString;
                                                      NSString *finalCautionString;
                                                      NSString *timePointString;
                                                      NSString *overallTimePoint;
                                                      
                                                      //convert laps to string to be saved
                                                      if(self.lapArray.count == 1){
                                                          
                                                          finalLapString = [NSString stringWithFormat:@"%f", [[self.lapArray objectAtIndex:0] doubleValue]];
                                                          
                                                      }else{
                                                          finalLapString = [NSString stringWithFormat:@"%f", [[self.lapArray objectAtIndex:0] doubleValue]];
                                                          
                                                          for (int x = 1; x < self.lapArray.count; x++) {
                                                              
                                                              finalLapString = [NSString stringWithFormat:@"%@, %f", finalLapString, [[self.lapArray objectAtIndex:x] doubleValue]];
                                                          }
                                                          
                                                      }
                                                      
                                                      //convert caution points to string to be saved
                                                      if(self.cautionArray.count == 0){
                                                          
                                                          finalCautionString = @"";
                                                      }
                                                      else if(self.cautionArray.count == 1)
                                                      {
                                                          finalCautionString = [NSString stringWithFormat:@"%f", [[self.cautionArray objectAtIndex:0] doubleValue]];
                                                      }
                                                      else{
                                                          finalCautionString = [NSString stringWithFormat:@"%f", [[self.cautionArray objectAtIndex:0] doubleValue]];
                                                          
                                                          for (int x = 1; x < self.cautionArray.count; x++) {
                                                              
                                                              finalCautionString = [NSString stringWithFormat:@"%@, %f", finalCautionString, [[self.cautionArray objectAtIndex:x] doubleValue]];
                                                          }
                                                          
                                                      }
                                                      
                                                      //Convert Time points to a string to be saved
                                                      if(self.timePointsArray.count == 0){
                                                          
                                                          timePointString = @"";
                                                          
                                                      }
                                                      else if(self.timePointsArray.count == 1)
                                                      {
                                                          timePointString = [NSString stringWithFormat:@"%f", [[self.timePointsArray objectAtIndex:0] doubleValue]];
                                                      }
                                                      else{
                                                          timePointString = [NSString stringWithFormat:@"%f", [[self.timePointsArray objectAtIndex:0] doubleValue]];
                                                          
                                                          for (int x = 1; x < self.timePointsArray.count; x++) {
                                                              
                                                              timePointString = [NSString stringWithFormat:@"%@, %f", timePointString, [[self.timePointsArray objectAtIndex:x] doubleValue]];
                                                          }
                                                      }
                                                      
                                                      //Convert Overal Time points to a string to be saved
                                                      if(self.overallTimeStamps.count == 0){
                                                          
                                                          overallTimePoint = @"";
                                                          
                                                      }
                                                      else if(self.overallTimeStamps.count == 1)
                                                      {
                                                          
                                                          overallTimePoint = [NSString stringWithFormat:@"%f", [[self.overallTimeStamps objectAtIndex:0] doubleValue]];
                                                      }
                                                      else{
                                                          overallTimePoint = [NSString stringWithFormat:@"%f", [[self.overallTimeStamps objectAtIndex:0] doubleValue]];
                                                          
                                                          for (int x = 1; x < self.overallTimeStamps.count; x++) {
                                                              
                                                              overallTimePoint = [NSString stringWithFormat:@"%@, %f", overallTimePoint, [[self.overallTimeStamps objectAtIndex:x] doubleValue]];
                                                          }
                                                          
                                                      }
                                                      
                                                      
                                                      
                                                      //Need to check for a any current Video is they have set it up in the race info page before the race
                                                      
                                                      //NEED TO LOOK AT THIS SAVING OF INFO
                                                      
                                                      assetToSave = asset;
                                                      NSString *tempTitle = [[NSString alloc] init];
                                                      NSString *tempRound = [[NSString alloc] init];
                                                      NSString *tempTrack = [[NSString alloc] init];
                                                     
                                                     CurrentVideo *temp = [CurrentVideo MR_findFirst];
                                                      if(temp.title.length > 0)
                                                      {
                                                          tempTitle = temp.title;
                                                      }
                                                      if(temp.track.length > 0)
                                                      {
                                                          tempTrack = temp.track;
                                                      }
                                                      if(temp.round.length > 0)
                                                      {
                                                          tempRound = temp.round;
                                                      }
                                                      
                                                      NSLog(@"TITLE: %@, TRACK: %@, ROUND: %@", tempTitle, tempTrack, tempRound);
                                                      
                                                      
                                                      [temp MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
                                                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                                                         
                                                          NSLog(@"DELE");
                                                      }];
                                                       
                                                      
                                                    /*  if(temp)
                                                      {
                                                          NSLog(@"TempVideo: %@", temp);
                                                      
                                                          //Saves the video to local storage- current video may have been set up already if Race Info was clicked before the race begin recording
                                                          CurrentVideo *video = [CurrentVideo MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
                                                          video.videoId = date;
                                                          video.videoDate = temp.videoDate;
                                                          video.track = temp.track;
                                                          video.round = temp.round;
                                                          video.bestLap = [NSNumber numberWithDouble: self.bestLapTime];
                                                          video.title = temp.title;
                                                          video.laps = finalLapString;
                                                          video.url = [[assetToSave valueForProperty:ALAssetPropertyAssetURL] absoluteString];
                                                          video.latitude = [NSNumber numberWithDouble: self.currentLocation.coordinate.latitude];
                                                          video.longitude = [NSNumber numberWithDouble:self.currentLocation.coordinate.longitude];
                                                          video.cautions = finalCautionString;
                                                          video.overallTimePoints = overallTimePoint;
                                                          video.videoTimePoints = timePointString;
                                                      
                                                      
                                                      }
                                                      else{*/
                                                          
                                                          
                                                         // NSLog(@"NO CURRENT VIDEO SAVED");
                                                          //Saves the video to local storage- current video may have been set up already if Race Info was clicked before the race begin recording
                                                          CurrentVideo *video = [CurrentVideo MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
                                                          video.videoId = date;
                                                          video.videoDate = date;
                                                            if(tempTrack.length > 0){
                                                                video.track = tempTrack;
                                                            }else{
                                                                video.track = @"";
                                                            }
                                                      
                                                            if(tempRound.length > 0){
                                                                video.round = tempRound;
                                                            }else{
                                                                video.round = @"";
                                                            }
                                                            if(tempTitle.length > 0){
                                                                video.title = tempTitle;
                                                            }else{
                                                                video.title = @"";
                                                            }
                                                    
                                                          video.bestLap = [NSNumber numberWithDouble: self.bestLapTime];
                                                          video.laps = finalLapString;
                                                          video.url = [[assetToSave valueForProperty:ALAssetPropertyAssetURL] absoluteString];
                                                          video.latitude = [NSNumber numberWithDouble: self.currentLocation.coordinate.latitude];
                                                          video.longitude = [NSNumber numberWithDouble:self.currentLocation.coordinate.longitude];
                                                          video.cautions = finalCautionString;
                                                          video.overallTimePoints = overallTimePoint;
                                                          video.videoTimePoints = timePointString;
                                                      
                                                      //Saves the current video and begins to save the saved video with the correct video ID
                                                      
                                                      
                                                      
                                                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                                                          NSLog(@"Save Current: %@", video);
                                                       
                                                          SavedVideo *videoSave = [SavedVideo MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
                                                          videoSave.videoId = date;
                                                          NSLog(@"VideoSave: %@", videoSave.videoId);
                                                          videoSave.url = [[assetToSave valueForProperty:ALAssetPropertyAssetURL] absoluteString];
                                                          videoSave.videoDate = date;
                                                          videoSave.title = @"No Title Recorded";
                                                          videoSave.track = @"No Track Recorded";
                                                          videoSave.round = @"No Round Recorded";
                                                          videoSave.bestLap = [NSNumber numberWithDouble: self.bestLapTime];
                                                          videoSave.latitude = [NSNumber numberWithDouble: self.currentLocation.coordinate.latitude];
                                                          videoSave.longitude = [NSNumber numberWithDouble: self.currentLocation.coordinate.longitude];
                                                          
                                                          videoSave.laps = finalLapString;
                                                          videoSave.cautions = finalCautionString;
                                                          videoSave.overallTimePoints = overallTimePoint;
                                                          videoSave.videoTimePoints = timePointString;
                                                          
                                                          
                                                          //Once the video data is saved into local storage the view and temp data is all reset and the view is transffered to the the Race Info page to make any adjustments before saving finally
                                                          [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                                                              if (success) {
                                                                  // NSLog(@"New Beacon Added to Local Storage: %@", newBeacon);
                                                                  [self.lapArray removeAllObjects];
                                                                  [self.timePointsArray removeAllObjects];
                                                                  [self.cautionArray removeAllObjects];
                                                                  [self.overallTimeStamps removeAllObjects];
                                                                  [spinner stopAnimating];
                                                                  [spinner removeFromSuperview];
                                                                  
                                                                  //Goes to the Race Info Page
                                                                  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                  RaceInfoViewController *infoView = [storyboard instantiateViewControllerWithIdentifier:@"RaceInfo"];
                                                                  [self presentViewController:infoView animated:YES completion:nil];
                                                                  
                                                              }
                                                          }];
                                                          
                                                      }];
                                                      
                                                  }
                                                 failureBlock:^(NSError* error) {
                                                     NSLog(@"failed to retrieve image asset:\nError: %@ ", [error localizedDescription]);
                                                 }];
                                     }
                                     else {
                                         
                                         NSLog(@"saved image failed.\nerror code %ld\n%@", (long)error.code, [error localizedDescription]);
                                     }
                                     
                                 }
                             }];
                        }
                        
                    }];
                });
            }
        }
    }
}



-(void) showBlackOutScreen {
    
    blackOutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [blackOutView setBackgroundColor:[UIColor blackColor]];
    [blackOutView setAlpha:0.65f];
    [self.view addSubview:blackOutView];
    [self.view bringSubviewToFront:blackOutView];
    

    [self.blackOutView addSubview: self.recordingSymbol];
    [self.blackOutView addSubview: self.cancelButton];
    [self.blackOutView addSubview: self.greenFinishButton];
    
    
    
}

-(void) finishedRecordingPressed {
    
    //----- STOP RECORDING -----
    NSLog(@"STOP RECORDING");
    self.finalMarkTime = [NSDate timeIntervalSinceReferenceDate];
    [self.finishButton setHidden:YES];
    [self.cautionButton setHidden:YES];
    [self.greenFinishButton setHidden:NO];
    [self.cancelButton setHidden:NO];

    [self showBlackOutScreen];
    
   // UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Finished" message:@"Are you finished recording or would you like to continue?" delegate:self cancelButtonTitle:@"Keep Racing" otherButtonTitles:@"Finished", nil];
   // [alertView show];
}

-(void) stopRecording {
    
    [self.bestLapLabel setText:@"00:00:00"];
    WeAreRecording = NO;
    [self setUpRecordingView:NO];
    [self.view bringSubviewToFront:spinner];
    [self.view bringSubviewToFront:savingVideoLabel];
    [self.cancelButton setHidden:YES];
    [self.greenFinishButton setHidden:YES];
    [MovieFileOutput stopRecording];
    
}


- (void) setAudioFormat:(CMFormatDescriptionRef) fmt
{
    const AudioStreamBasicDescription *asbd = CMAudioFormatDescriptionGetStreamBasicDescription(fmt);
    _samplerate = asbd->mSampleRate;
    _channels = asbd->mChannelsPerFrame;
    
}


- (void) captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    BOOL bVideo = YES;
    
    @synchronized(self)
    {
        if (!self.WeAreRecording  || self.isPaused)
        {
            return;
        }
        if (connection != _videoConnection)
        {
            bVideo = NO;
        }
        if ((self.encoder == nil) && !bVideo)
        {
            CMFormatDescriptionRef fmt = CMSampleBufferGetFormatDescription(sampleBuffer);
            [self setAudioFormat:fmt];
            NSString* filename = [NSString stringWithFormat:@"capture%d.mp4", _currentFile];
            NSString* path = [NSTemporaryDirectory() stringByAppendingPathComponent:filename];
            self.encoder = [VideoEncoder encoderForPath:path Height:_cy width:_cx channels:_channels samples:_samplerate];
        }
        if (_discont)
        {
            if (bVideo)
            {
                return;
            }
            _discont = NO;
            // calc adjustment
            CMTime pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            CMTime last = bVideo ? _lastVideo : _lastAudio;
            if (last.flags & kCMTimeFlags_Valid)
            {
                if (_timeOffset.flags & kCMTimeFlags_Valid)
                {
                    pts = CMTimeSubtract(pts, _timeOffset);
                }
                CMTime offset = CMTimeSubtract(pts, last);
                NSLog(@"Setting offset from %s", bVideo?"video": "audio");
                NSLog(@"Adding %f to %f (pts %f)", ((double)offset.value)/offset.timescale, ((double)_timeOffset.value)/_timeOffset.timescale, ((double)pts.value/pts.timescale));
                
                // this stops us having to set a scale for _timeOffset before we see the first video time
                if (_timeOffset.value == 0)
                {
                    _timeOffset = offset;
                }
                else
                {
                    _timeOffset = CMTimeAdd(_timeOffset, offset);
                }
            }
            _lastVideo.flags = 0;
            _lastAudio.flags = 0;
        }
        
        // retain so that we can release either this or modified one
        CFRetain(sampleBuffer);
        
        if (_timeOffset.value > 0)
        {
            CFRelease(sampleBuffer);
            sampleBuffer = [self adjustTime:sampleBuffer by:_timeOffset];
        }
        
        // record most recent time so we know the length of the pause
        CMTime pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
        CMTime dur = CMSampleBufferGetDuration(sampleBuffer);
        if (dur.value > 0)
        {
            pts = CMTimeAdd(pts, dur);
        }
        if (bVideo)
        {
            _lastVideo = pts;
        }
        else
        {
            _lastAudio = pts;
        }
    }
    
    // pass frame to encoder
    [self.encoder encodeFrame:sampleBuffer isVideo:bVideo];
    CFRelease(sampleBuffer);
}


- (CMSampleBufferRef) adjustTime:(CMSampleBufferRef) sample by:(CMTime) offset
{
    CMItemCount count;
    CMSampleBufferGetSampleTimingInfoArray(sample, 0, nil, &count);
    CMSampleTimingInfo* pInfo = malloc(sizeof(CMSampleTimingInfo) * count);
    CMSampleBufferGetSampleTimingInfoArray(sample, count, pInfo, &count);
    for (CMItemCount i = 0; i < count; i++)
    {
        pInfo[i].decodeTimeStamp = CMTimeSubtract(pInfo[i].decodeTimeStamp, offset);
        pInfo[i].presentationTimeStamp = CMTimeSubtract(pInfo[i].presentationTimeStamp, offset);
    }
    CMSampleBufferRef sout;
    CMSampleBufferCreateCopyWithNewTiming(nil, sample, count, pInfo, &sout);
    free(pInfo);
    return sout;
}



//********** DID FINISH RECORDING TO OUTPUT FILE AT URL **********
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput
didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
	  fromConnections:(NSArray *)connections
				error:(NSError *)error
{
    
	NSLog(@"didFinishRecordingToOutputFileAtURL - enter");
   
    BOOL RecordedSuccessfully = YES;
    if ([error code] != noErr)
	{
        // A problem occurred: Find out if the recording was successful.
        id value = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];
        if (value)
		{
            RecordedSuccessfully = [value boolValue];
        }
    }
	if (RecordedSuccessfully)
	{
		//----- RECORDED SUCESSFULLY -----
        NSLog(@"didFinishRecordingToOutputFileAtURL - success");
		ALAssetsLibrary *library = [self defaultAssetsLibrary];
  
        __block ALAssetsGroup* groupToAddTo;
        [library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                    usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                        if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString: @"Race Note"]) {
                                            NSLog(@"found album %@", @"Race Note");
                                            groupToAddTo = group;
                                        }
                                        else{
                                            
                                            NSLog(@"NO Race Note Album");
                                        }
                                    }
                                  failureBlock:^(NSError* error) {
                                      NSLog(@"failed to enumerate albums:\nError: %@", [error localizedDescription]);
                                  }];
		if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputFileURL])
            
		{
        }
		
	}
}


- (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

-(void) setUpRecordingView: (BOOL)showRecordingOrNot {
    
    //This is what is shown to the user when the app is video recording
    if(showRecordingOrNot == YES){
    
       // [self.startStopButton setImage:[UIImage imageNamed:@"checkered-flag.png"] forState:UIControlStateNormal];
        
        [self.timerBar setHidden:NO];
        [self.finishButton setHidden:NO];
        [self.startButton setHidden:YES];
        [self.recordingSymbol setImage:[UIImage imageNamed: @"recording.png"]];
        [self.startStopLabelOutlet setText:@"Stop"];
        //[self.lapLabelOutlet setHidden: NO];
        
        [self.raceInfoButtonOutlet setEnabled:NO];
        [self.myRacesButtonOutlet setEnabled:NO];
        
        [self.addTimePointOutlet setHidden:NO];
        [self.addTimePointOutlet setEnabled:YES];
        [self.lapButton setHidden: NO];
       // [self.videoNoteLabelOutlet setHidden:NO];
       // [self.videoNoteButtonOutlet setHidden:NO];
       // [self.cautionLabelOutlet setHidden:NO];
        [self.cautionButton setHidden:NO];
        //[self.myRacesLabelOutlet setHidden: YES];
        [self.myRacesButtonOutlet setHidden:YES];
        [self.raceInfoButtonOutlet setHidden:YES];
        //[self.raceInfoButtonOutlet setHidden:YES];
        //Best Lap will not show until the user hits lap
       // if(self.lapArray.count == 0){
        //
         //   self.bestLapLabel.hidden = YES;
       // }
       // else{
            
         //    self.bestLapLabel.hidden = NO;
            
        //}
        
        //self.currentLapLabel.hidden = NO;
        
        
    }
    else{ //View that is showing when not recording
        
        //[self.startStopButton setImage:[UIImage imageNamed:@"green-flag.png"] forState:UIControlStateNormal];
         [self.timerBar setHidden:YES];
        [self.raceInfoButtonOutlet setEnabled:YES];
        [self.myRacesButtonOutlet setEnabled:YES];
           [self.addTimePointOutlet setEnabled:NO];
        [self.addTimePointOutlet setHidden:YES];
        [self.finishButton setHidden:YES];
        [self.startButton setHidden:NO];
        [self.recordingSymbol setImage:nil];
        [self.startStopLabelOutlet setText:@"Start"];
        [self.lapLabelOutlet setHidden: YES];
        [self.lapButton setHidden: YES];
       // [self.videoNoteLabelOutlet setHidden:YES];
       // [self.videoNoteButtonOutlet setHidden:YES];
       // [self.cautionLabelOutlet setHidden:YES];
        [self.cautionButton setHidden:YES];
        //[self.myRacesLabelOutlet setHidden: NO];
        [self.myRacesButtonOutlet setHidden:NO];
        [self.raceInfoButtonOutlet setHidden:NO];
        [self.raceInfoButtonOutlet setHidden:NO];
       // self.bestLapLabel.hidden = YES;
       // self.currentLapLabel.hidden = YES;
       
        
    }
    
}


//This method uses the current AVcaptureDevice to zoom in or out depending on its current position
- (IBAction)zoomInOut:(id)sender {
    
    if(!zoomedIn){
        
        float zoomLevel = 5.0;
        float zoomRate = 3;
        
        if ([VideoInputDevice.device lockForConfiguration:nil]) {
            [VideoInputDevice.device rampToVideoZoomFactor:zoomLevel withRate:zoomRate];
            [VideoInputDevice.device unlockForConfiguration];
        }
        self.zoomedIn = YES;
        
    }
    else{
        float zoomLevel = 1.0;
        float zoomRate = 3.5;
        
            if ([VideoInputDevice.device lockForConfiguration:nil]) {
                [VideoInputDevice.device rampToVideoZoomFactor:zoomLevel withRate:zoomRate];
                [VideoInputDevice.device unlockForConfiguration];
            }
        
            self.zoomedIn = NO;
        }
        
}


-(void)lapClicked {
    
    [self.lapButton setAlpha:0.35];
    
    [self saveLap: 0];
    
}

-(void) saveLap: (int)tag {
    
  //  [self.bestLapLabel setHidden:NO];
    //Checks it recording is in session to avoid a crash
    if(WeAreRecording == NO){
        
        return;
    }
    
    
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval lapTime;
    
    //The "time" variable gets reset to the current time before updating the timer.
    if(tag == 0){
        lapTime = currentTime - time;
    }
    else{
        
        lapTime = finalMarkTime - time;
    }
   
    
    //Makes the first object a lap once the first lap has been clicked
    if(self.lapArray.count == 0){
        
        self.bestLapTime = lapTime;
    }
    
    //Resets the time to the currentTime and adds the lapTime object to the lapArray
    time = currentTime;
    [self.lapArray addObject: [NSNumber numberWithDouble: lapTime]];
    [self.overallTimeStamps addObject:[NSNumber numberWithDouble:0]];
    [self.overallTimeStamps addObject:[NSNumber numberWithDouble:lapTime]];
    //Resets the Current Lap timer back to zero and starts counting up for that lap
    [self updateTimer];
    
    //Checks the current lap time against the best and resets if need be
    if(lapTime < bestLapTime)
    {
        self.bestLapTime = lapTime;
    }
    NSLog(@"lapArray: %@", self.lapArray);
    //Converts the lap time into readable seconds, minutes, and hours
   // int hours = (int)(bestLapTime/ 3600);
    int minutes = (int)(bestLapTime / 60.0);
    int seconds = (int)(bestLapTime = bestLapTime - (minutes * 60));
     int milliseconds = (int)(bestLapTime * 100) % 100;
    
    NSString *currentLapTime
    = [NSString stringWithFormat:@"%02d:%02d:%02d", minutes, seconds, milliseconds];
    
    //Sets the best label
    [self.bestLapLabel setText: currentLapTime];
    
    [self performSelector:@selector(resetLapButton) withObject:nil afterDelay:0.185];
    
}

-(void) resetLapButton {
    
    [self.lapButton setAlpha:1.0f];
}

-(void)saveCaution: (double)startCautionTime :(double)endTime
{
    
    double caution = (endTime - startCautionTime);
 
    [self.cautionArray addObject:[NSNumber numberWithDouble:caution]];
    [self.overallTimeStamps addObject:[NSNumber numberWithDouble:1]];
    [self.overallTimeStamps addObject:[NSNumber numberWithDouble:caution]];
    self.time = [NSDate timeIntervalSinceReferenceDate];
    [self updateTimer];
    NSLog(@"Caution Time: %@", cautionArray);
    
    
}
- (IBAction)raceInfoClicked:(id)sender {
    
    NSLog(@"Race Info Clicked");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RaceInfoViewController *infoView = [storyboard instantiateViewControllerWithIdentifier:@"RaceInfo"];
    [self presentViewController:infoView animated:YES completion:nil];
    [infoView raceInfoViewFromCapture];
    
    
}
- (IBAction)myRacesButtonClicked:(id)sender {
    NSLog(@"My Races Pressed");
}
- (void)cautionButtonPressed {
    
    if(!isPaused){

        [self pauseCapture];
        [self setCautionViewOn:YES];
        self.isPaused = YES;
        [self saveLap:0];
        self.cautionTime = [NSDate timeIntervalSinceReferenceDate];
    }

}
        
         
-(void) setCautionViewOn: (BOOL)cautionViewOnOrOff {
    
    if(cautionViewOnOrOff){
        
        [self.timerBar setBackgroundColor:[UIColor colorWithRed:(245/255.f) green:(195/255.f) blue:(0/255.f) alpha:1.0f]];
        [self.timerBar setAlpha: 1.0f];
        [self.currentLapLabel setHidden:NO];
        [self.bestLapLabel setHidden:YES];
        [self.recordingSymbolView setHidden:YES];
        [self.notRecordingView setHidden:NO];
        
        [self.recordingSymbolView setContentMode:UIViewContentModeScaleAspectFit];
        [self.undoStartRaceButton setHidden:YES];
        [self.currentLabel setHidden:YES];
        [self.bestLabel setHidden:YES];
        [self.stopwatch setHidden:YES];
        [self.cautionButton setEnabled:NO];
        [self.finishButton setEnabled:NO];
        [self.cautionButton setAlpha:0.01f];
        [self.finishButton setAlpha:0.05f];
        [self.lapButton setHidden:YES];
        [self.startButton setHidden:NO];
        [self.touchStartLabel setHidden:NO];
        [self.cautionLabel setHidden:NO];
        [self.cautionFlag setHidden:NO];
        [self.cautionTimeLabel setHidden:NO];
        
    }
    else{ //Do The opposite of above
        
        [self.timerBar setBackgroundColor:[UIColor blackColor]];
         [self.timerBar setAlpha:0.7f];
        //[self.timerBar setAlpha: 1.0f];
        [self.currentLapLabel setHidden:NO];
        [self.bestLapLabel setHidden:NO];
        [self.recordingSymbolView setImage: [UIImage imageNamed:@"recording.png"]];
        [self.recordingSymbolView setHidden:NO];
        [self.notRecordingView setHidden:YES];
        
        [self.recordingSymbolView setContentMode:UIViewContentModeScaleAspectFit];
        [self.undoStartRaceButton setHidden:NO];
        [self.currentLabel setHidden:NO];
        [self.bestLabel setHidden:NO];
        [self.stopwatch setHidden:NO];
        [self.cautionButton setEnabled:YES];
        [self.finishButton setEnabled:YES];
        [self.cautionButton setAlpha:1.01f];
        [self.finishButton setAlpha:1.0f];
        [self.lapButton setHidden:NO];
        [self.startButton setHidden:YES];
        [self.touchStartLabel setHidden:YES];
        [self.cautionLabel setHidden:YES];
        [self.cautionFlag setHidden:YES];
        [self.cautionTimeLabel setHidden:YES];
    }
    
             
}

-(void)greenFinishedButtonPressed{
    NSLog(@"Green Finsihed Pressed;");
    
    [self.startStopButton setEnabled:NO];
    [self.startStopLabelOutlet setEnabled:NO];
    [self.lapLabelOutlet setEnabled: NO];
    [self.lapButtonOutlet setEnabled: NO];
    [self.myRacesButtonOutlet setEnabled: NO];
    [self.raceInfoButtonOutlet setEnabled: NO];
    [self.zoomButtonOutlet setEnabled:NO];
    [self.bestLapLabel setEnabled:NO];
    [self.currentLapLabel setEnabled: NO];
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    [spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner startAnimating];
    
    [self.savingVideoLabel setHidden:NO];
    [self saveLap: 1];
    [self stopCapture: YES];
    [self.greenFinishButton setEnabled:NO];
    [self.cancelButton setEnabled:NO];
}

-(void) cancelButtonPressed {
    NSLog(@"Cancel Pressed");
    finalMarkTime = 0;
    [self.cautionButton setHidden:NO];
    [self.finishButton setHidden:NO];
    [self.blackOutView removeFromSuperview];
}


- (void)handlePinchFrom:(UIPinchGestureRecognizer *)recognizer
{
    float zoomLevel = recognizer.scale;
    float zoomRate = recognizer.velocity;
  
    NSLog(@"Scale: %f, Velocity: %f", zoomLevel, zoomRate);
    if (zoomLevel <= 1) {
        zoomLevel = 1;
    }
    if(zoomRate < 0){
        
        zoomRate = abs(zoomRate);
        
    }

        if ([VideoInputDevice.device lockForConfiguration:nil]) {
            [VideoInputDevice.device rampToVideoZoomFactor:zoomLevel withRate:zoomRate];
            [VideoInputDevice.device unlockForConfiguration];
        }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 0){
        
        finalMarkTime = 0;
    }
    else{
    
        //Not used anymore
     /*   [self.startStopButton setEnabled:NO];
        [self.startStopLabelOutlet setEnabled:NO];
        [self.lapLabelOutlet setEnabled: NO];
        [self.lapButtonOutlet setEnabled: NO];
        
        
       // [self.videoNoteLabelOutlet setEnabled: NO];
        //[self.videoNoteButtonOutlet setEnabled: NO];
       // [self.cautionLabelOutlet setEnabled: fNO];
       // [self.cautionButtonOutlet setEnabled: NO];
       // [self.myRacesLabelOutlet setEnabled: NO];
        [self.myRacesButtonOutlet setEnabled: NO];
        //[self.raceInfoLabelOutlet setEnabled: NO];
        [self.raceInfoButtonOutlet setEnabled: NO];
        [self.zoomButtonOutlet setEnabled:NO];
        [self.bestLapLabel setEnabled:NO];
        [self.currentLapLabel setEnabled: NO];
        [self.view addSubview:spinner];
        [self.view bringSubviewToFront:spinner];
     
        [spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [spinner startAnimating];
           [self.savingVideoLabel setHidden:NO];
        [self saveLap: 1];
        [self stopRecording];*/
        
    }
}

#pragma mark Location Manager Delegates

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    if(locations){
        
        self.currentLocation = locations[0];
        NSLog(@"Current Location %@", self.currentLocation);
        [locationManager stopUpdatingLocation];
        
    }
}


- (IBAction)addTimePointClicked:(id)sender {
    
    NSLog(@"Add Time Point Clicked");
    [self.view makeToast:@"Time Point Added"
                duration:1.7
                position:CSToastPositionBottom];
    
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    [self.timePointsArray addObject:[NSNumber numberWithDouble:currentTime - startTime]];
    [self.overallTimeStamps addObject:[NSNumber numberWithDouble:2]];
    [self.overallTimeStamps addObject:[NSNumber numberWithDouble:currentTime - startTime]];
    
    
}
- (IBAction)undoStartRace:(id)sender {
    
    NSLog(@"Unstart Race");
    [self stopCapture: NO];
}

- (void) pauseCapture
{
    @synchronized(self)
    {
        if (self.WeAreRecording)
        {
            NSLog(@"Pausing capture");
            self.isPaused = YES;
            _discont = YES;
        }
    }
}

- (void) resumeCapture
{
    @synchronized(self)
    {
        if (self.isPaused)
        {
            NSLog(@"Resuming capture");
            self.isPaused = NO;
        }
    }
}

@end
