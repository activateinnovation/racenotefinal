//
//  MoreInfoViewController.m
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 9/29/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "MoreInfoViewController.h"

@interface MoreInfoViewController ()

@end

@implementation MoreInfoViewController
@synthesize segmentController;
@synthesize lapsArray;
@synthesize bestLap;
@synthesize videoAsset;
@synthesize savedVideo;
@synthesize videoTimePoints;
@synthesize cautionsArray;
@synthesize overallTags;
@synthesize overallTimePoints;
@synthesize lapCount;
@synthesize cautionCount;
@synthesize videoTimePointCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.lapCount = 0;
    self.cautionCount = 0;
    self.videoTimePointCount = 0;
    
    UIView *orangeBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [orangeBar setBackgroundColor:[UIColor colorWithRed:(241/255.f) green:(106/255.f) blue:(39/255.f) alpha:1.0]];
    [self.view addSubview: orangeBar];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getVideoData: (NSDate *)videoId :(ALAsset *)asset {
    
    self.lapsArray = [[NSMutableArray alloc] init];
    self.videoTimePoints = [[NSMutableArray alloc] init];
    self.overallTimePoints = [[NSMutableArray alloc] init];
    self.overallTags = [[NSMutableArray alloc] init];
    self.videoAsset = asset;
    self.savedVideo = [SavedVideo MR_findFirstByAttribute:@"videoId" withValue:videoId];
    //NSLog(@"More Info --- SavedVideo: %@, VideoAsset: %@", savedVideo, videoAsset);
    self.lapsArray = [[savedVideo.laps componentsSeparatedByString:@","] mutableCopy];
    self.videoTimePoints  = [[savedVideo.videoTimePoints componentsSeparatedByString:@","] mutableCopy];
    self.cautionsArray = [[savedVideo.cautions componentsSeparatedByString:@","] mutableCopy];
    if(self.cautionsArray.count > 0)
    {
        if([self.cautionsArray[0] isEqualToString:@""]){
            
            [self.cautionsArray removeAllObjects];
        }
    }
    
    
    NSLog(@"Cautions Array: %@", self.cautionsArray);
    NSMutableArray *temp = [[savedVideo.overallTimePoints componentsSeparatedByString:@","] mutableCopy];
    for(int x = 0; x < temp.count; x+=2){
        
        [self.overallTags addObject:[temp objectAtIndex:x]];
        [self.overallTimePoints addObject:[temp objectAtIndex:x+1]];
        
    }
    
    
    
    NSLog(@"OverTags: %@  OverallTime: %@", overallTags, overallTimePoints);
}

- (IBAction)backToVideoPlayback:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VideoPlaybackViewController *infoView = [storyboard instantiateViewControllerWithIdentifier:@"VideoPlayback"];
    [self presentViewController:infoView animated:YES completion:nil];
    
    [infoView getVideoData: self.savedVideo.videoId : self.videoAsset];
}


#pragma mark start of TABLEVIEW DELEGATE


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([segmentController selectedSegmentIndex] == 0)
    {
        return overallTags.count;
    }
    else if([segmentController selectedSegmentIndex] == 1){
        return self.lapsArray.count;
    }
    else if([segmentController selectedSegmentIndex] == 2){
        
        //  NSLog(@"Video Notes: %@", videoTimePoints);
        if(videoTimePoints.count == 1){
            
            if([[videoTimePoints objectAtIndex:0] isEqualToString:@""])
            {
                return 0;
            }
            
        }
        return videoTimePoints.count;
    }
    else{
        
        return cautionsArray.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
    }
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    
    
    if([segmentController selectedSegmentIndex] == 0)
    {
        
        if([[self.overallTags objectAtIndex:indexPath.row] intValue] == 0){
            
            //The Date of this asset will pull infortmation from stored Video Objects that have more info like the date as the key, the url, the number of the movie, any laps or cautions.
            NSLog(@"Saved Video3: %@", savedVideo);
            // lapCount++;
            double lapTime = [[overallTimePoints objectAtIndex:indexPath.row] doubleValue];
            
            //int hours = (int)(lapTime/ 3600);
            int minutes = (int)(lapTime / 60.0);
            int seconds = (int)(lapTime = lapTime - (minutes * 60));
            int milliseconds = (int)(lapTime * 100) % 100;
            
            NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02d", minutes, seconds, milliseconds];
            
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(465, 15, 200, 30)];
            [timeLabel setFont:[UIFont boldSystemFontOfSize:18]];
            [timeLabel setText: currentTimeString];//[NSString stringWithFormat:@"%.02fs", [[lapsArray objectAtIndex:indexPath.row] doubleValue]]];
            
            
            
            UILabel *lapLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 150, 30)];
            [lapLabel setFont:[UIFont boldSystemFontOfSize:20]];
            [lapLabel setText: [NSString stringWithFormat:@"Lap %d", (int)indexPath.row + 1]];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(427, 14, 30, 30)];
            [imageView setImage:[UIImage imageNamed:@"trophy-orange.png"]];
            
            //  NSLog(@"%f", [[NSString stringWithFormat:@"%.02f", [[lapsArray objectAtIndex:indexPath.row] doubleValue]] doubleValue]);
            
            if([[NSString stringWithFormat:@"%.02f", [[overallTimePoints objectAtIndex:indexPath.row] doubleValue]] doubleValue] == [[NSString stringWithFormat:@"%.02f", [self.savedVideo.bestLap doubleValue]] doubleValue]){
                
                [cell addSubview:imageView];
                
            }
            
            [cell addSubview:lapLabel];
            [cell addSubview:timeLabel];
            
        }
        else if([[self.overallTags objectAtIndex:indexPath.row] intValue] == 2)
        {
            double videoNoteTime = [[overallTimePoints objectAtIndex:indexPath.row] doubleValue];
            
            //videoTimePointCount++;
            
            //int hours = (int)(lapTime/ 3600);
            int minutes = (int)(videoNoteTime / 60.0);
            int seconds = (int)(videoNoteTime = videoNoteTime - (minutes * 60));
            int milliseconds = (int)(videoNoteTime * 100) % 100;
            
            NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02d", minutes, seconds, milliseconds];
            
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(465, 15, 200, 30)];
            [timeLabel setFont:[UIFont boldSystemFontOfSize:18]];
            [timeLabel setText: currentTimeString];//[NSString stringWithFormat:@"%.02fs", [[lapsArray objectAtIndex:indexPath.row] doubleValue]]];
            
            
            
            UILabel *videoNoteLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 150, 30)];
            [videoNoteLabel setFont:[UIFont boldSystemFontOfSize:20]];
            [videoNoteLabel setText: [NSString stringWithFormat:@"Video Note %d", (int)indexPath.row + 1]];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(427, 17, 25, 25)];
            [imageView setImage:[UIImage imageNamed:@"note-gray.png"]];
            
            
            
            [cell addSubview:imageView];
            [cell addSubview:videoNoteLabel];
            [cell addSubview:timeLabel];
        }
        else
        {
            
            // cautionCount++;
            double cautionTime = [[overallTimePoints objectAtIndex:indexPath.row] doubleValue];
            
            //int hours = (int)(lapTime/ 3600);
            int minutes = (int)(cautionTime / 60.0);
            int seconds = (int)(cautionTime = cautionTime - (minutes * 60));
            int milliseconds = (int)(cautionTime * 100) % 100;
            
            NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02d", minutes, seconds, milliseconds];
            
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(465, 15, 200, 30)];
            [timeLabel setFont:[UIFont boldSystemFontOfSize:18]];
            [timeLabel setText: currentTimeString];//[NSString stringWithFormat:@"%.02fs", [[lapsArray objectAtIndex:indexPath.row] doubleValue]]];
            
            
            
            UILabel *cautionLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 150, 30)];
            [cautionLabel setFont:[UIFont boldSystemFontOfSize:20]];
            [cautionLabel setText: [NSString stringWithFormat:@"Caution %d", (int)indexPath.row + 1]];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(427, 17, 25, 25)];
            [imageView setImage:[UIImage imageNamed:@"flag-yellow.png"]];
            
            
            
            [cell addSubview:imageView];
            [cell addSubview:cautionLabel];
            [cell addSubview:timeLabel];
            
            
        }
        
        
    }
    else if([segmentController selectedSegmentIndex] == 1){
        
        //The Date of this asset will pull infortmation from stored Video Objects that have more info like the date as the key, the url, the number of the movie, any laps or cautions.
        NSLog(@"Saved Video4: %@", savedVideo);
        
        double lapTime = [[lapsArray objectAtIndex:indexPath.row] doubleValue];
        
        //int hours = (int)(lapTime/ 3600);
        int minutes = (int)(lapTime / 60.0);
        int seconds = (int)(lapTime = lapTime - (minutes * 60));
        int milliseconds = (int)(lapTime * 100) % 100;
        
        NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02d", minutes, seconds, milliseconds];
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(465, 15, 200, 30)];
        [timeLabel setFont:[UIFont boldSystemFontOfSize:18]];
        [timeLabel setText: currentTimeString];//[NSString stringWithFormat:@"%.02fs", [[lapsArray objectAtIndex:indexPath.row] doubleValue]]];
        
        
        UILabel *lapLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 150, 30)];
        [lapLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [lapLabel setText: [NSString stringWithFormat:@"Lap %d", (int)(indexPath.row + 1)]];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(427, 14, 30, 30)];
        [imageView setImage:[UIImage imageNamed:@"trophy-orange.png"]];
        
        NSLog(@"%f", [[NSString stringWithFormat:@"%.02f", [[lapsArray objectAtIndex:indexPath.row] doubleValue]] doubleValue]);
        
        if([[NSString stringWithFormat:@"%.02f", [[lapsArray objectAtIndex:indexPath.row] doubleValue]] doubleValue] == [[NSString stringWithFormat:@"%.02f", [self.savedVideo.bestLap doubleValue]] doubleValue]){
            
            [cell addSubview:imageView];
            
        }
        
        [cell addSubview:lapLabel];
        [cell addSubview:timeLabel];
        
    }
    else if([ segmentController selectedSegmentIndex] == 2)
    {
        
        NSLog(@"Video Notes: %@",videoTimePoints);
        double videoNoteTime = [[videoTimePoints objectAtIndex:indexPath.row] doubleValue];
        
        //int hours = (int)(lapTime/ 3600);
        int minutes = (int)(videoNoteTime / 60.0);
        int seconds = (int)(videoNoteTime = videoNoteTime - (minutes * 60));
        int milliseconds = (int)(videoNoteTime * 100) % 100;
        
        NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02d", minutes, seconds, milliseconds];
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(465, 15, 200, 30)];
        [timeLabel setFont:[UIFont boldSystemFontOfSize:18]];
        [timeLabel setText: currentTimeString];//[NSString stringWithFormat:@"%.02fs", [[lapsArray objectAtIndex:indexPath.row] doubleValue]]];
        
        
        
        UILabel *videoNoteLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 150, 30)];
        [videoNoteLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [videoNoteLabel setText: [NSString stringWithFormat:@"Video Note %d", (int)(indexPath.row + 1)]];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(427, 17, 25, 25)];
        [imageView setImage:[UIImage imageNamed:@"note-gray.png"]];
        
        
        
        [cell addSubview:imageView];
        [cell addSubview:videoNoteLabel];
        [cell addSubview:timeLabel];
    }
    else
    {
        
        
        double cautionTime = [[cautionsArray objectAtIndex:indexPath.row] doubleValue];
        
        NSLog(@"Caution Time: %f", cautionTime);
        
        //int hours = (int)(lapTime/ 3600);
        int minutes = (int)(cautionTime / 60.0);
        int seconds = (int)(cautionTime = cautionTime - (minutes * 60));
        int milliseconds = (int)(cautionTime * 100) % 100;
        
        NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d:%02d", minutes, seconds, milliseconds];
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(465, 15, 200, 30)];
        [timeLabel setFont:[UIFont boldSystemFontOfSize:18]];
        [timeLabel setText: currentTimeString];//[NSString stringWithFormat:@"%.02fs", [[lapsArray objectAtIndex:indexPath.row] doubleValue]]];
        
        
        
        UILabel *cautionLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 150, 30)];
        [cautionLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [cautionLabel setText: [NSString stringWithFormat:@"Caution %d", (int)(indexPath.row + 1)]];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(427, 17, 25, 25)];
        [imageView setImage:[UIImage imageNamed:@"flag-yellow.png"]];
        
        
        
        [cell addSubview:imageView];
        [cell addSubview:cautionLabel];
        [cell addSubview:timeLabel];
        
        
    }
    
    
    
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // NSLog(@"Video Note: %@", [self.videoTimePoints objectAtIndex:indexPath.row]);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VideoNoteViewController *viewController = [storyboard instantiateViewControllerWithIdentifier: @"videoNotes"];
    
        
        
        if([segmentController selectedSegmentIndex] == 0)
        {
            
            if([[self.overallTags objectAtIndex:indexPath.row] intValue] == 0){
                
                //The Date of this asset will pull infortmation from stored Video Objects that have more info like the date as the key, the url, the number of the movie, any laps or cautions.
              //  NSLog(@"Saved Video1: %@", savedVideo);
                // lapCount++;
                double lapTime = [[overallTimePoints objectAtIndex:indexPath.row] doubleValue];
                //LAPTIME POINT - tag 1
                [viewController getVideoData:self.savedVideo.videoId :lapTime :1];
            }
            else if([[self.overallTags objectAtIndex:indexPath.row] intValue] == 2)
            {
                double videoNoteTime = [[overallTimePoints objectAtIndex:indexPath.row] doubleValue];
                //VIDEOTIME POINT - tag = 2
                //videoTimePointCount++;
                [viewController getVideoData:self.savedVideo.videoId : videoNoteTime :2];
            }
            else
            {
                
                // cautionCount++;
                double cautionTime = [[overallTimePoints objectAtIndex:indexPath.row] doubleValue];
                //CAUTION TIME - tag = 3
                [viewController getVideoData:self.savedVideo.videoId :cautionTime :3];
                
                
            }
            
            
        }
        else if([segmentController selectedSegmentIndex] == 1){
            
            //The Date of this asset will pull infortmation from stored Video Objects that have more info like the date as the key, the url, the number of the movie, any laps or cautions.
           // NSLog(@"Saved Video2: %@", savedVideo);
            
            double lapTime = [[lapsArray objectAtIndex:indexPath.row] doubleValue];
            //LAP TIME POINT - tag = 1
            [viewController getVideoData:self.savedVideo.videoId :lapTime :1];
        }
        else if([ segmentController selectedSegmentIndex] == 2)
        {
            
           // NSLog(@"Video Notes: %@",videoTimePoints);
            double videoNoteTime = [[videoTimePoints objectAtIndex:indexPath.row] doubleValue];
            //Video Time Point - tag = 2
            [viewController getVideoData:self.savedVideo.videoId :videoNoteTime :2];
            
        }
        else
        {
            
            
            double cautionTime = [[cautionsArray objectAtIndex:indexPath.row] doubleValue];
            //Caution Time Point - tag = 3
            [viewController getVideoData:self.savedVideo.videoId :cautionTime :3];
            
        }
        
        [self presentViewController:viewController animated:YES completion:nil];

    
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        

}


- (IBAction)segmentValueChanged:(id)sender {
    
    NSLog(@"Sender Value: %d", (int)[sender selectedSegmentIndex]);
    self.lapCount = 0;
    self.cautionCount = 0;
    self.videoTimePointCount = 0;
    [self.tableView reloadData];
    
}
@end
