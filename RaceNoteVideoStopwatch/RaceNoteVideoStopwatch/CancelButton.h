//
//  CancelButton.h
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 10/28/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelButton : UIButton

@end
