//
//  VideoNote.h
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 12/6/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface VideoNote : NSManagedObject

@property (nonatomic, retain) NSDate * videoId;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSNumber * time;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * position;

@end
