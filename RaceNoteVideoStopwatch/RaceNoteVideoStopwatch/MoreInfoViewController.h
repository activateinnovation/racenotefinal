//
//  MoreInfoViewController.h
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 9/29/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import "SavedVideo.h"
#import "VideoCaptureViewController.h"
#import "VideoNoteViewController.h"


@class VideoNote;
@class VideoNoteViewController;

@interface MoreInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backToVideoPlayback:(id)sender;
@property (strong, nonatomic) NSMutableArray *lapsArray;
@property (strong, nonatomic) NSMutableArray *videoTimePoints;
@property (strong, nonatomic) NSMutableArray *cautionsArray;
@property (strong, nonatomic) NSMutableArray *overallTags;
@property (strong, nonatomic) NSMutableArray *overallTimePoints;
@property double bestLap;
-(void) getVideoData: (NSDate *)videoId :(ALAsset *)asset;
@property (strong, nonatomic) ALAsset *videoAsset;
@property (strong, nonatomic) SavedVideo *savedVideo;
- (IBAction)segmentValueChanged:(id)sender;
@property int lapCount;
@property int cautionCount;
@property int videoTimePointCount;

@end
