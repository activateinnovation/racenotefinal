//
//  startButton.m
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 10/21/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "StartButton.h"

@implementation StartButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // do my additional initialization here
        [self layoutSubviews];
    }
    return self;
}

- (void)layoutSubviews {
   
    [super layoutSubviews];
    
    // Get the size of the button
    CGRect bounds = self.bounds;
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height)];
    CAShapeLayer *circleLayer = [[CAShapeLayer alloc]init];
    circleLayer.fillColor = [UIColor colorWithRed:(29/255.f) green:(180/255.f) blue:(87/255.f) alpha:1.0].CGColor;
    circleLayer.strokeColor = [UIColor whiteColor].CGColor;
    circleLayer.path = bezierPath.CGPath;
    circleLayer.lineWidth = 0.8;
    
    UIImageView *startFlag = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.origin.x + bounds.size.width/4.3, bounds.origin.y + bounds.size.height/5.5, 27, 27)];
    //[startFlag setBackgroundColor:[UIColor clearColor]];
    [startFlag setImage:[UIImage imageNamed:@"flag-green-dark.png"]];
    
    // Adding circle shapelayer as a subLayer to UILabel's layer
    UILabel *startStopLabelOutlet = [[UILabel alloc] init];
    [startStopLabelOutlet setText:@"Start"];
    [startStopLabelOutlet setTextColor:[UIColor whiteColor]];
    [startStopLabelOutlet setFont:[UIFont fontWithName:@"ProximaNova-BoldIt" size:18]];
    
    //[startStopLabelOutlet setFont:[UIFont boldSystemFontOfSize:18]];
    [startStopLabelOutlet setTextAlignment:NSTextAlignmentCenter];
    [startStopLabelOutlet setFrame:CGRectMake(bounds.origin.x + 5, bounds.origin.y + bounds.size.height/3.6, 100, 40)];
    
    
    [self.layer addSublayer:circleLayer];
    
    
    
    [self addSubview: startStopLabelOutlet];
    [self bringSubviewToFront:startStopLabelOutlet];
    [self addSubview: startFlag];
    [self bringSubviewToFront:startFlag];

}



@end
