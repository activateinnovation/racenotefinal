//
//  LapButton.m
//  RaceNoteVideoStopwatch
//
//  Created by Taylor Korensky on 10/21/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "LapButton.h"

@implementation LapButton


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // do my additional initialization here
        [self layoutSubviews];
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    // Get the size of the button
    CGRect bounds = self.bounds;
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height)];
    CAShapeLayer *circleLayer = [[CAShapeLayer alloc]init];
    circleLayer.fillColor = [UIColor colorWithRed:(25/255.f) green:(151/255.f) blue:(197/255.f) alpha:1.0].CGColor;
    circleLayer.strokeColor = [UIColor whiteColor].CGColor;
    circleLayer.path = bezierPath.CGPath;
    circleLayer.lineWidth = 0.8;
    
    UIImageView *lapTimer = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.origin.x + bounds.size.width/4.3, bounds.origin.y + bounds.size.height/5.5, 27, 27)];
    //[startFlag setBackgroundColor:[UIColor clearColor]];
    [lapTimer setImage:[UIImage imageNamed:@"lap-blue-dark.png"]];
    
    // Adding circle shapelayer as a subLayer to UILabel's layer
    UILabel *lapLabel = [[UILabel alloc] init];
    [lapLabel setText:@"Lap"];
    [lapLabel setTextColor:[UIColor whiteColor]];
    
    [lapLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [lapLabel setTextAlignment:NSTextAlignmentCenter];
    [lapLabel setFrame:CGRectMake(bounds.origin.x + 5, bounds.origin.y + bounds.size.height/3.6, 100, 40)];
    [lapLabel setFont:[UIFont fontWithName:@"ProximaNova-BoldIt" size:18]];
    
    [self.layer addSublayer:circleLayer];
    
    
    [self addSubview: lapLabel];
    [self bringSubviewToFront:lapLabel];
    [self addSubview: lapTimer];
    [self bringSubviewToFront:lapTimer];
    
}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
