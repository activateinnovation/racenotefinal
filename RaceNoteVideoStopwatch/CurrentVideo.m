//
//  CurrentVideo.m
//  RaceNoteVideoStopwatch
//
//  Created by Test on 9/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "CurrentVideo.h"


@implementation CurrentVideo

@dynamic bestLap;
@dynamic laps;
@dynamic round;
@dynamic track;
@dynamic videoDate;
@dynamic videoId;
@dynamic url;
@dynamic latitude;
@dynamic longitude;
@dynamic videoTimePoints;
@dynamic cautions;
@dynamic overallTimePoints;
@dynamic title;
@end
